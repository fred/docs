.. role:: raw-html-m2r(raw)
   :format: html

==================
FRED Documentation
==================

This documentation aids users of the `Free Registry for ENUM and
Domains <https://fred.nic.cz>`_\ , the domain name registry software
developed by CZ.NIC.

The documentation contains the following publications:


* Features,
* Concepts,
* Architecture Description,
* Administration Manual,
* EPP Reference Manual,
* RDAP API Reference.

The newest edition of the documentation is published at https://fred.nic.cz/documentation.

What is in development
======================

Parts of the docs which are currently in development by CZ.NIC:


* To be planned.

Contributions
=============

We accept pull requests.

History
=======

The ``master`` always contains the newest edition of the documentation of the
last FRED release.
The documentation of older FRED releases is available under tags such as ``v2.33``.
The oldest historical version tagged is ``v2.29``.

License
=======

See `LICENSE.md <LICENSE.md>`_.

.. image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
   :target: http://creativecommons.org/licenses/by-sa/4.0/
   :alt: Creative Commons License
