.. _FRED-Admin-Install-Ubuntu:

Installation on Ubuntu
----------------------

This section explains the individual steps that need to be taken
to install software required for the operation of FRED.

Switch to root before you begin

  ``sudo su -``

FRED installation
^^^^^^^^^^^^^^^^^

Installation steps are split as they would be performed node by node. 
Nodes are described in chapter :ref:`Distributed deployment example <FRED-Arch-Deployment>`

DB node
~~~~~~~

.. code-block:: bash

   # OS is up to you, postgresql 13 is required

   apt update
   apt install -y ca-certificates curl gnupg lsb-release

   # Add cznic keyring for fred packages
   mkdir -p /usr/share/keyrings/
   wget https://archive.nic.cz/dists/cznic-archive-keyring.gpg --output-document=/usr/share/keyrings/cznic-archive-keyring.gpg

   # Add source list for FRED
   cat << EOT >> /etc/apt/sources.list.d/fred.list
   deb [signed-by=/usr/share/keyrings/cznic-archive-keyring.gpg] http://archive.nic.cz/public $(lsb_release -sc) main
   EOT

   # Create preferences.d/ pin list to install correct fred packages (current APT preferences file is available at https://fred.nic.cz/en/get-fred/)
   wget https://fred.nic.cz/media/filer_public/71/ce/71ce3145-a4bb-4583-9ff2-218627d71d5f/20241fredpreferencesd.txt -O /etc/apt/preferences.d/fred

   # Postfix non-interactive installation
   debconf-set-selections <<< "postfix postfix/mailname string $(hostname)"
   debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
   apt --assume-yes install postfix

   # Install postgresql 13
   wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
   echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" | sudo tee  /etc/apt/sources.list.d/pgdg.list
   apt update
   apt -y install postgresql-13 postgresql-client-13

   apt install fred-db

   # Latest postgresql version is always installed with fred-db – it is not required and can be uninstalled (only psql 13 is needed)
   apt remove -y postgresql-17 postgresql-client-17
   rm -r /etc/postgresql/17

   sed -i 's#/12/#/13/#g' /usr/sbin/fred-dbmanager

   # FRED DB CREATION AND MIGRATIONS
   su - postgres -c "/usr/sbin/fred-dbmanager install"

   # MESSENGER DB CREATION - CHANGE CREDENTIALS!
   sudo -u postgres psql -c 'CREATE DATABASE messenger;'
   sudo -u postgres psql -c "CREATE USER messenger WITH ENCRYPTED PASSWORD 'password';"
   sudo -u postgres psql -c 'GRANT ALL PRIVILEGES ON DATABASE messenger TO messenger;'

   # SECRETARY DB CREATION - CHANGE CREDENTIALS!
   sudo -u postgres psql -c 'CREATE DATABASE secretary;'
   sudo -u postgres psql -c "CREATE USER secretary WITH ENCRYPTED PASSWORD 'passwd';"
   sudo -u postgres psql -c 'GRANT ALL PRIVILEGES ON DATABASE secretary TO secretary;'

   # FERDA DB CREATION - CHANGE CREDENTIOALS!
   sudo -u postgres psql -c 'CREATE DATABASE ferda;'
   sudo -u postgres psql -c "CREATE USER ferda WITH ENCRYPTED PASSWORD 'password';"
   sudo -u postgres psql -c 'GRANT ALL PRIVILEGES ON DATABASE ferda TO ferda;'

APP node
~~~~~~~~

.. code-block:: bash

   # Ubuntu 20.04 LTS

   # Install dependencies
   apt update
   apt install -y ca-certificates curl gnupg lsb-release python3-dnspython python3-pip uwsgi-plugin-python3

   # Add cznic keyring for fred packages
   mkdir -p /usr/share/keyrings/
   wget https://archive.nic.cz/dists/cznic-archive-keyring.gpg --output-document=/usr/share/keyrings/cznic-archive-keyring.gpg

   # Add source list for FRED
   cat << EOT >> /etc/apt/sources.list.d/fred.list
   deb [signed-by=/usr/share/keyrings/cznic-archive-keyring.gpg] http://archive.nic.cz/public $(lsb_release -sc) main
   EOT

   # Postfix non-interactive installation
   debconf-set-selections <<< "postfix postfix/mailname string $(hostname)"
   debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
   apt --assume-yes install postfix

   # Create preferences.d/ pin list to install correct fred packages (current APT preferences file is available at https://fred.nic.cz/en/get-fred/)
   wget https://fred.nic.cz/media/filer_public/71/ce/71ce3145-a4bb-4583-9ff2-218627d71d5f/20241fredpreferencesd.txt -O /etc/apt/preferences.d/fred

   apt update

   apt -y install fred-idl fred-typist python3-pydantic fred-api-logger fred-api-fileman fred-backend-logger fred-backend-logger-corba fred-backend-registry fred-backend-fileman fred-backend-notify fred-backend-public-request fred-backend-zone  fred-backend-dbreport fred-rifd fred-pifd fred-adifd fred-akmd fred-accifd fred-akm cdnskey-scanner python3-fred-epplib

   # Now you can configure installed fred components. Some packages create their own config automatically in /etc/fred, in some cases you may need to create the config file yourself. Most config file examples are available in https://gitlab.nic.cz/fred/demo-install/-/tree/master/files/configs or in the project repository itself (such as https://gitlab.nic.cz/fred/server/-/blob/master/config/server.conf.in)

   /usr/sbin/fred-admin --registrar_add --handle=REG-SYSTEM --reg_name=REG-SYSTEM --organization=SYSTEM --street1=SYSTEM --city=SYSTEM --email=SYSTEM --url=SYSTEM --country=CZ --dic=12345 --no_vat --system

   # You can change the location of logs to any path using fred configuration
   chown 666 /var/log/fred.log

   # Enable and start FRED services
   systemctl daemon-reload
   systemctl enable --now omniorb4-nameserver fred-accifd fred-adifd fred-akmd fred-backend-logger fred-logger-corba fred-backend-registry fred-pifd fred-rifd fred-fileman-server fred-zone-services fred-dbreport-services@messenger.service fred-dbreport-services@registry.service fred-dbreport-services@fredlog.service

   # Disable unused FRED services
   systemctl disable fred-auctions-warehouse
   systemctl mask fred-auction-warehouse.service
   systemctl disable fred-dbreport-services.service
   systemctl mask fred-dbreport-services.service

   # Restart the services to be sure, that the config is correctly loaded
   systemctl restart 'fred-*'
   systemctl restart omniorb4-nameserver

   # If services fail to start check journalctl and systemd. Service file contains information on how the daemon is run, so you might want to try running it manually to get more specific error messages if logs don't help.
   # dpkg -L fred-backend-registry | grep ".service"
   # /lib/systemd/system/fred-backend-registry.service
   # grep -i exec /lib/systemd/system/fred-backend-registry.service
   # ExecStart=/usr/sbin/fred-registry-services --config /etc/fred/fred-registry-services.conf
   # sudo -u fred /usr/sbin/fred-registry-services --config /etc/fred/fred-registry-services.conf

EPP node
~~~~~~~~

.. code-block:: bash

   # Ubuntu 20.04 LTS

   apt update
   apt install -y ca-certificates curl gnupg lsb-release python3-pip apache2

   # Add cznic keyring for fred packages
   mkdir -p /usr/share/keyrings/
   wget https://archive.nic.cz/dists/cznic-archive-keyring.gpg --output-document=/usr/share/keyrings/cznic-archive-keyring.gpg

   # Add source list for FRED
   cat << EOT >> /etc/apt/sources.list.d/fred.list
   deb [signed-by=/usr/share/keyrings/cznic-archive-keyring.gpg] http://archive.nic.cz/public $(lsb_release -sc) main
   EOT

   # Postfix non-interactive installation
   debconf-set-selections <<< "postfix postfix/mailname string $(hostname)"
   debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
   apt --assume-yes install postfix

   # Create preferences.d/ pin list to install correct fred packages (current APT preferences file is available at https://fred.nic.cz/en/get-fred/)
   wget https://fred.nic.cz/media/filer_public/71/ce/71ce3145-a4bb-4583-9ff2-218627d71d5f/20241fredpreferencesd.txt -O /etc/apt/preferences.d/fred

   apt update

   apt -y install libapache2-mod-corba libapache2-mod-eppd python3-fred-epplib

   # Enable apache2 modules required by EPP
   a2enmod corba eppd ssl

   # Enable eppd site in apache
   a2ensite 02-fred-mod-eppd-apache

   # Do not forget to edit configuration to suit your needs (/etc/apache2/sites-enabled/), then you can restart

   systemctl restart apache2

ADMIN node
~~~~~~~~~~

.. code-block:: bash

   # OS is up to you, docker apps only

   apt update
   apt install -y ca-certificates curl gnupg lsb-release python3-pip

   # Postfix non-interactive installation
   debconf-set-selections <<< "postfix postfix/mailname string $(hostname)"
   debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
   apt --assume-yes install postfix

   sudo mkdir -m 0755 -p /etc/apt/keyrings

   curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --batch --yes --dearmor -o /etc/apt/keyrings/docker.gpg

   echo \
   "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

   apt update
   apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

   # Create dummy network interface for docker
   cat << EOT >> /etc/netplan/01-dock.yaml
   network:
   version: 2
   renderer: networkd
   bridges:
      dock:
         dhcp4: false
         dhcp6: false
         accept-ra: false
         interfaces: [ ]
         addresses:
         - 192.168.1.1/32
   EOT

   netplan apply

   # Create ferda configuration in this folder (example conf can be found on https://gitlab.nic.cz/fred/ferda/-/tree/master/docs/demo-deploy, full list of configurable options is here https://gitlab.nic.cz/fred/ferda/-/blob/master/README.rst?ref_type=heads)
   mkdir -p /etc/fred/ferda/

   # It depends if you plan to have more admin nodes or just one (if one, keep the 127.0.0.1)
   docker swarm init --advertise-addr 127.0.0.1

   # Pull docker images
   docker pull registry.nic.cz/fred/ferda/ferda-uwsgi
   docker pull registry.nic.cz/fred/ferda/ferda-nginx

   cd /etc/fred/ferda

   # Deploy docker stacks
   docker stack deploy --compose-file docker-compose.yml ferda

   # Check that ferda container is running - docker ps | grep ferda_uwsgi, should be up for about 20+ seconds

   # Run DB migrations - DB needs to be correctly configured in .env
   docker exec  $(docker ps | grep ferda_uwsgi.1 | cut -d ' ' -f 1) django-admin migrate

   # Change superuser credentials as needed
   docker exec -e DJANGO_SUPERUSER_USERNAME=admin -e DJANGO_SUPERUSER_EMAIL=admin@admin.example -e DJANGO_SUPERUSER_PASSWORD=password $(docker ps | grep ferda_uwsgi.1 | cut -d ' ' -f 1) django-admin createsuperuser --noinput

WEB node
~~~~~~~~

.. code-block:: bash

   # Ubuntu 20.04 LTS
   # Accesible from the internet, public services on this node

   apt update
   apt install -y ca-certificates curl gnupg lsb-release python3-pip apache2

   # Add cznic keyring for fred packages
   mkdir -p /usr/share/keyrings/
   wget https://archive.nic.cz/dists/cznic-archive-keyring.gpg --output-document=/usr/share/keyrings/cznic-archive-keyring.gpg

   # Add source list for FRED
   cat << EOT >> /etc/apt/sources.list.d/fred.list
   deb [signed-by=/usr/share/keyrings/cznic-archive-keyring.gpg] http://archive.nic.cz/public $(lsb_release -sc) main
   EOT

   # Postfix non-interactive installation
   debconf-set-selections <<< "postfix postfix/mailname string $(hostname)"
   debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
   apt --assume-yes install postfix

   sudo mkdir -m 0755 -p /etc/apt/keyrings

   curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --batch --yes --dearmor -o /etc/apt/keyrings/docker.gpg

   echo \
   "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

   apt update
   apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

   # Create dummy network interface for docker
   cat << EOT >> /etc/netplan/01-dock.yaml
   network:
   version: 2
   renderer: networkd
   bridges:
      dock:
         dhcp4: false
         dhcp6: false
         accept-ra: false
         interfaces: [ ]
         addresses:
         - 192.168.1.1/32
   EOT

   netplan apply

   apt -y install libapache2-mod-corba libapache2-mod-whoisd

   # Enable apache2 modules required by WHOIS
   a2enmod corba whoisd ssl

   # Enable whois site in apache
   a2ensite 02-fred-mod-whoisd-apache

   systemctl restart apache2

   # Create webwhois and rdap configuration in these folders(example confs can be found on https://gitlab.nic.cz/fred/rdap/-/tree/master/docs/demo-deploy?ref_type=heads and https://gitlab.nic.cz/fred/webwhois/-/tree/master/docs/demo-deploy)
   mkdir -p /etc/fred/rdap/
   mkdir -p /etc/fred/webwhois/

   # It depends if you plan to have more admin nodes or just one (if one, keep the 127.0.0.1)
   docker swarm init --advertise-addr 127.0.0.1

   # Pull docker images
   docker pull registry.nic.cz/fred/rdap/rdap-uwsgi
   docker pull registry.nic.cz/fred/rdap/rdap-nginx

   docker pull registry.nic.cz/fred/webwhois/webwhois-uwsgi
   docker pull registry.nic.cz/fred/webwhois/webwhois-nginx

   # Deploy docker stacks
   cd /etc/fred/rdap
   docker stack deploy --compose-file docker-compose.yml rdap

   cd /etc/fred/webwhois
   docker stack deploy --compose-file docker-compose.yml webwhois

HM node
~~~~~~~

.. code-block:: bash

   apt update
   apt install -y ca-certificates curl gnupg lsb-release python3-pip apache2

   # Add cznic keyring for fred packages
   mkdir -p /usr/share/keyrings/
   wget https://archive.nic.cz/dists/cznic-archive-keyring.gpg --output-document=/usr/share/keyrings/cznic-archive-keyring.gpg

   # Add source list for FRED
   cat << EOT >> /etc/apt/sources.list.d/fred.list
   deb [signed-by=/usr/share/keyrings/cznic-archive-keyring.gpg] http://archive.nic.cz/public $(lsb_release -sc) main
   EOT

   # Postfix non-interactive installation
   debconf-set-selections <<< "postfix postfix/mailname string $(hostname)"
   debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
   apt --assume-yes install postfix

   # Create preferences.d/ pin list to install correct fred packages (current APT preferences file is available at https://fred.nic.cz/en/get-fred/)
   wget https://fred.nic.cz/media/filer_public/71/ce/71ce3145-a4bb-4583-9ff2-218627d71d5f/20241fredpreferencesd.txt -O /etc/apt/preferences.d/fred

   apt update

   apt -y install fred-zone-generator

   # You will need to create your own configuration, example is here https://gitlab.nic.cz/fred/backend/zone/-/blob/master/server-example.conf?ref_type=heads), restart service after change

SECRETARY node
~~~~~~~~~~~~~~

.. code-block:: bash

   # Ubuntu 20.04 LTS

   apt update
   apt install -y ca-certificates curl gnupg lsb-release python3-pip uwsgi uwsgi-plugin-python3 nginx

   # Add cznic keyring for fred packages
   mkdir -p /usr/share/keyrings/
   wget https://archive.nic.cz/dists/cznic-archive-keyring.gpg --output-document=/usr/share/keyrings/cznic-archive-keyring.gpg

   # Add source list for FRED
   cat << EOT >> /etc/apt/sources.list.d/fred.list
   deb [signed-by=/usr/share/keyrings/cznic-archive-keyring.gpg] http://archive.nic.cz/public $(lsb_release -sc) main
   EOT

   # Create preferences.d/ pin list to install correct fred packages (current APT preferences file is available at https://fred.nic.cz/en/get-fred/)
   wget https://fred.nic.cz/media/filer_public/71/ce/71ce3145-a4bb-4583-9ff2-218627d71d5f/20241fredpreferencesd.txt -O /etc/apt/preferences.d/fred

   # Postfix non-interactive installation
   debconf-set-selections <<< "postfix postfix/mailname string $(hostname)"
   debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
   apt --assume-yes install postfix

   apt update

   apt -y install python3-django-secretary

   # Enable secretary in UWSGI
   cp /usr/share/doc/python3-django-secretary/examples/fred-secretary.ini /etc/uwsgi/apps-available/
   ln -s /etc/uwsgi/apps-available/fred-secretary.ini /etc/uwsgi/apps-enabled/fred-secretary.ini
   sed -i '/\/run\/uwsgi\/fred-secretary\/socket/c\socket = \/run\/uwsgi\/app\/fred-secretary\/socket' /etc/uwsgi/apps-available/fred-secretary.ini
   cp /usr/share/doc/python3-django-secretary/examples/uwsgi_secretary.py /etc/uwsgi/

   # Setup nginx and copy secretary configuration
   cp /usr/share/doc/python3-django-secretary/examples/secretary-nginx.conf /etc/nginx/sites-available/secretary.conf
   cp /usr/share/doc/python3-django-secretary/examples/fred-secretary.service /etc/systemd/system
   ln -s /etc/nginx/sites-available/secretary.conf /etc/nginx/sites-enabled/secretary.conf
   rm /etc/nginx/sites-enabled/default

   # Create a folder for the UWSGI socket
   mkdir -p /run/uwsgi/app/fred-secretary/
   chown www-data:www-data /run/uwsgi/app/fred-secretary
   # Create static files folder for secretary
   mkdir -p /var/www/fred/
   chown www-data:www-data /var/www/fred

   # Before you run any secretary command, configuration must exist – example conf can be found here https://gitlab.nic.cz/fred/demo-install/-/tree/master/files/configs/secretary_cfg

   sudo -u www-data PYTHONPATH=/etc/fred DJANGO_SETTINGS_MODULE=secretary_cfg.settings django-admin migrate

   # Collect secretary static files
   sudo -u www-data PYTHONPATH=/etc/fred DJANGO_SETTINGS_MODULE=secretary_cfg.settings django-admin collectstatic

   # Create secretary super user
   sudo -u www-data DJANGO_SUPERUSER_USERNAME=admin DJANGO_SUPERUSER_PASSWORD=password DJANGO_SUPERUSER_EMAIL=admin@admin.example PYTHONPATH=/etc/fred DJANGO_SETTINGS_MODULE=secretary_cfg.settings django-admin createsuperuser --noinput

   # Load secretary templates
   apt install python3-docopt
   cd /usr/share/doc/python3-django-secretary/examples/secretary-templates/
   sudo -u www-data PYTHONPATH=/etc/fred DJANGO_SETTINGS_MODULE=secretary_cfg.settings python3 load_templates.py pdf-templates.yml
   sudo -u www-data PYTHONPATH=/etc/fred DJANGO_SETTINGS_MODULE=secretary_cfg.settings python3 load_templates.py fred-migration.yml
   sudo -u www-data PYTHONPATH=/etc/fred DJANGO_SETTINGS_MODULE=secretary_cfg.settings python3 load_templates.py fred-templates.yml

   systemctl daemon-reload
   systemctl enable --now fred-secretary
   systemctl restart nginx

MESSENGER node
~~~~~~~~~~~~~~

.. code-block:: bash

   apt update
   apt install -y ca-certificates curl gnupg lsb-release python3-pip uwsgi uwsgi-plugin-python3 nginx
   pip install aioitertools

   # Add cznic keyring for fred packages
   mkdir -p /usr/share/keyrings/
   wget https://archive.nic.cz/dists/cznic-archive-keyring.gpg --output-document=/usr/share/keyrings/cznic-archive-keyring.gpg

   # Add source list for FRED
   cat << EOT >> /etc/apt/sources.list.d/fred.list
   deb [signed-by=/usr/share/keyrings/cznic-archive-keyring.gpg] http://archive.nic.cz/public $(lsb_release -sc) main
   EOT

   # Create preferences.d/ pin list to install correct fred packages (current APT preferences file is available at https://fred.nic.cz/en/get-fred/)
   wget https://fred.nic.cz/media/filer_public/71/ce/71ce3145-a4bb-4583-9ff2-218627d71d5f/20241fredpreferencesd.txt -O /etc/apt/preferences.d/fred

   # Postfix non-interactive installation
   debconf-set-selections <<< "postfix postfix/mailname string $(hostname)"
   debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
   apt --assume-yes install postfix

   apt update

   apt -y install fred-api-fileman fred-api-messenger fred-backend-messenger

   MESSENGER_CONFIG=/etc/fred/messenger.conf alembic --config /etc/fred/messenger-alembic.ini upgrade head

   systemctl daemon-reload
   systemctl enable --now fred-backend-messenger fred-messenger-server

