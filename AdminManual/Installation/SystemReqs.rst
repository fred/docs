
.. _system-reqs:

System requirements
-------------------

This section describes the environment suitable for running the FRED.

Hardware
^^^^^^^^

We recommend to run the FRED on **64-bit** architectures.

Binaries are available only for 64-bit architectures.

Sources can be compiled for both 32-bit and 64-bit architectures
but the 32-bit variant is no longer tested or supported.


Operating system
^^^^^^^^^^^^^^^^^

To deploy binaries, you must have installed this **operating system**:

* Ubuntu 20.04 LTS (Focal Fossa)

To compile sources from tarballs, you may try to use any Linux distribution
of your choice, but we tested the compilation/installation procedure
only on Ubuntu.


.. _system-reqs-aux:

Auxiliary software
^^^^^^^^^^^^^^^^^^

.. NOTE "large programs" that must run concurrently with the FRED

To make use of all FRED features, the following auxiliary programs are required:

* (essential) CORBA naming server (OmniNames),
* (essential) PostgreSQL database server (PG>=13),
* (essential) Apache web server (Apache>2.2),
* mail server (Postfix, Sendmail, Exim or other),
* DNS server (KnotDNS, Bind, NSD or other).

.. Note:: All of these tools are installed automatically when installing
   from binaries but they must be installed manually when you decide to compile
   source code yourself. But don't worry, the appropriate packages
   are included in the dependency lists for each supported operating system.
   