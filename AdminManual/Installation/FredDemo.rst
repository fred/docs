.. _FRED-Admin-Demo:

FRED demo
-----------

#. For testing and demonstration purposes we provide a virtual image of Ubuntu server
   with uninitialized instance of FRED. Download the image
   `here <https://fred-demo.nic.cz/>`_. For building the demo we use script in the
   repository :repo:`fred/demo-install`.
   
#. Import the ``.qcow2`` image.

#. After importing, you can launch the server and ssh into the machine
   with user: ``fred``, password: ``password``.

#. After successfully logging in, restart all registry services via command:

   .. code-block:: bash

      sudo systemctl restart 'fred-*'

   .. Important:: Registry itself is not initialized!
      Before you start working with FRED, you need to take steps described in chapter
      :ref:`registry initialization <FRED-Admin-RegInit>`. 

#. The registry is ready to use. You can find the following services running on these locations:
      * Browser:
          * FERDA – web based administration interface: https://localhost:4443 login ``admin`` / ``password``
          * Secretary – django admin app for editing mail templates: https://localhost:8090 login ``admin`` / ``password``
          * WebWHOIS – simple website for searching domains using whois protocol: https://localhost:4444
      * Protocols:
          * EPP http://localhost:700
          * WHOIS https://localhost:4444
          * RDAP https://localhost:4445
      * CLI Tools:
          * ``/usr/sbin/fred-admin`` – Administrate the registry, registrars and customise pricing
            (some of these actions are also available in more user friendly FERDA administration interface)
          * ``/usr/bin/fred-client`` – A Python EPP client for registrars to allow administration
            registry objects without having to write their own implementation of EPP 
          * C++ and python daemons as described :ref:`here <config-servers-cpp>`
      * Databases: To access the db cluster switch to user ``postgres`` using ``sudo su - postgres`` and running ``psql`` 

   .. Note:: Please note that for full functionality you should configure periodic
      tasks, as described in chapter :ref:`periodic tasks <FRED-Admin-PeriodicTasks>`.
