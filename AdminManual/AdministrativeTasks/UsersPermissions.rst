
Manage users and permissions
----------------------------

This section describes how to manage permissions, create users and
add security features in Ferda webadmin tool.

Permissions
^^^^^^^^^^^

Permissions are managed through the Django admin site, which is
separate from the main Ferda user interface, and can be accessed on the
:file:`/admin` path.

A user with access to the admin site must be given the *Staff status*
and can be designated to have all permissions implicitly with the *Superuser
status*.

User management permissions:

- ``Can view user``
- ``Can add user``
- ``Can change user``
- ``Can delete user``

Registry management permissions:

- ``Can view basic registry content`` -- required to display any information
  from the Registry
- ``Can view AuthInfo`` -- required to display the authorization information (AuthInfo)
- ``Can view registrar credit``

You may create groups of these permissions and assign them to users.


Users and groups example
~~~~~~~~~~~~~~~~~~~~~~~~

This example describes a model situation where we have two groups
of permissions: one for customer support team workers and another for customer
support team managers.

Groups

- a *Customer Support Lead* group might have all Registry management permissions
- a *Customer Support Worker* group might have only the permission
  ``Can view basic registry content``

Users

- users of customer support who require only basic permissions would be
  assigned the *Customer Support Worker* group
- a user of a customer support manager who requires extended permissions,
  would be assigned the *Customer Support Lead* group

Security features
^^^^^^^^^^^^^^^^^

You can add several security features to assure safety of the system, e.g. two-factor
authentication for login or automatic log out from Ferda.

Two-factor Authentication
~~~~~~~~~~~~~~~~~~~~~~~~~

Ferda offers a possibility to use a two-factor authentication via FIDO tokens. This highly increases security of the
administration system.

Before you enforce the second step of the authentication, you first need to set up the FIDO token at least for one admin.
Otherwise, you could get stuck in a situation where no one can log in. Because of that, the setup of the two-factor
authentication is divided into the following two steps.

First, you need to assign tokens to users (or at least one admin) in administration.

Then you can enforce the second step of authentication by setting up the ``FIDO_ENABLED=true`` in ``.env`` file when
:ref:`running the container<2factor auth enable>`.

Automatic log out
~~~~~~~~~~~~~~~~~

You can set an automatic log out from Ferda after a specified amount of time of inactivity.
In `.env` file change the variable `SESSION_COOKIE_AGE` to a desired value (in seconds).