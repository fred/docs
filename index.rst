..
.. FRED documentation master file
..

============================
FRED |version| Documentation
============================

This documentation aids various user groups of the **Free Registry for ENUM and Domains**,
the domain name registry software developed by `CZ.NIC <http://www.nic.cz>`_
as an open-source solution.

.. only:: mode_structure

   .. Note:: This deliverable was generated in the *structure mode*
      which means that it contains authoring/editing/managing notes
      in addition to the actual content.

:Edition: :doc:`1.4 <RecordOfChanges>`

:Source: The source code of this documentation is open and can be found
         on `GitLab <https://gitlab.nic.cz/fred/docs>`_.

         Contributions may be submitted using the pull-request mechanism.

:General:

   .. toctree::
      :name: fred-toc-info
      :maxdepth: 1

      RecordOfChanges
      LegalNotice
      TypographicConventions
      Glossary

.. only:: include_todolist and format_html

   :doc:`/TODOList`

.. toctree::
   :caption: Publications
   :name: fred-toc-publications
   :maxdepth: 2

   Features/index
   Concepts/index
   Architecture/index
   AdminManual/index
   EPPReference/index
   RDAPReference/index
   ReleaseNotes/index
