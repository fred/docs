
.. index::
   pair: check; domain

Check domain
============

A domain check :ref:`command <struct-command>` is used to check
the availability of one or more domain names.

The domain check command is a ``check`` element in the ``domain`` namespace
(``http://www.nic.cz/xml/epp/domain-1.4``).

The command must be contained in the ``<check>`` command type.

.. index:: Ⓔcheck, Ⓔname

Element structure without Auction module
----------------------------------------

Command element structure
^^^^^^^^^^^^^^^^^^^^^^^^^

The ``<domain:check>`` element must declare the ``domain`` :doc:`namespace and schema </EPPReference/SchemasNamespaces/index>` and it must contain the following child elements:

* ``<domain:name>`` **(1..n)**  – a domain name as :term:`eppcom:labelType`.

.. code-block:: xml
   :caption: Example

   <?xml version="1.0" encoding="UTF-8"?>
      <epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:ietf:params:xml:ns:epp-1.0 epp-1.0.xsd">
         <command>
               <check>
                  <domain:check xmlns:domain="http://www.nic.cz/xml/epp/domain-1.4" xsi:schemaLocation="http://www.nic.cz/xml/epp/domain-1.4 domain-1.4.5.xsd">
                     <domain:name>
                           available-domain.cz
                     </domain:name>
                     <domain:name>
                           registered-domain.cz
                     </domain:name>
                  </domain:check>
               </check>
               <clTRID>
                  nlr23s#2024-04-15T16:07:37.622471
               </clTRID>
         </command>
      </epp>

.. code-block:: shell
   :caption: FRED-eppic example

   > check-domain mydomain.cz somedomain.cz

.. index:: ⒺchkData, Ⓔcd, Ⓔname, Ⓔreason, ⓐavail, ⓐlang

Response element structure
^^^^^^^^^^^^^^^^^^^^^^^^^^

The :ref:`response <struct-response>` from the FRED EPP server contains
the result, response data, and transaction identification.

See also :ref:`succ-fail`.

The response data element (``<resData>``) contains a single child element
``<domain:chkData>`` which declares the ``domain`` :doc:`namespace and schema </EPPReference/SchemasNamespaces/index>`
and it contains the following child elements:

* ``<domain:cd>`` **(1..n)** – the check resolution of a single domain name:

   * ``<domain:name>`` **(1)** – the domain name as :term:`eppcom:labelType`,

      * ``@avail`` **(R)** – availability as :term:`xs:boolean`;
        ``true`` – available, ``false`` – not available,

   * ``<domain:reason>`` **(0..1)** – if the availability is negative,
     this element contains an explanation why the domain name is not available,
     as :term:`fredcom:msgType`.

      * ``@lang`` – language of the reason as :term:`xs:language`;
        default is ``en`` (English).

.. code-block:: xml
   :caption: Example

   <epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:ietf:params:xml:ns:epp-1.0 epp-1.0.xsd">
   <response>
      <result code="1000">
         <msg>Command completed successfully</msg>
      </result>
      <resData>
         <domain:chkData xmlns:domain="http://www.nic.cz/xml/epp/domain-1.4" xsi:schemaLocation="http://www.nic.cz/xml/epp/domain-1.4 domain-1.4.5.xsd">
         <domain:cd>
            <domain:name avail="1">available-domain.cz</domain:name>
         </domain:cd>
         <domain:cd>
            <domain:name avail="0">registered-domain.cz</domain:name>
            <domain:reason>Registered already</domain:reason>
         </domain:cd>
         </domain:chkData>
      </resData>
      <trID>
         <clTRID>nlr23s#2024-04-15T16:07:37.622471</clTRID>
         <svTRID>ReqID-0063282953</svTRID>
      </trID>
   </response>
   </epp>

.. code-block:: shell
   :caption: FRED-eppic example

   Command completed successfully (1000)

   data:
   - avail: true
   name: nic.cz
   reason: null
   - avail: false
   name: example.cz
   reason: Registered already

Element structure with Auction module
-------------------------------------

Command element structure
^^^^^^^^^^^^^^^^^^^^^^^^^

Since ``fred-2.50.0`` you can add an **optional** extension for the ``<domain:check>`` command if domain auctions are active.
If the domain can be registered by the winner and the extension contains the element ``<registrant>`` with the correct winner handle,
the command returns ``avail="1"``. If you do not use the extension or use non-winning handle, the response contains ``avail="0"`` and corresponding reason.

.. code-block:: xml
   :caption: Example

   <?xml version="1.0" encoding="UTF-8"?>
      <epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:ietf:params:xml:ns:epp-1.0 epp-1.0.xsd">
         <command>
               <check>
                  <domain:check xmlns:domain="http://www.nic.cz/xml/epp/domain-1.4" xsi:schemaLocation="http://www.nic.cz/xml/epp/domain-1.4 domain-1.4.5.xsd">
                     <domain:name>
                           available-domain.cz
                     </domain:name>
                     <domain:name>
                           registered-domain.cz
                     </domain:name>
                     <domain:name>
                           auction-pending.cz
                     </domain:name>
                     <domain:name>
                           is-auction-winner.cz
                     </domain:name>
                     <domain:name>
                           is-not-auction-winner.cz
                     </domain:name>
                  </domain:check>
               </check>
               <extension>
                  <check xmlns="http://www.nic.cz/xml/epp/auction-1.0" xsi:schemaLocation="http://www.nic.cz/xml/epp/auction-1.0 auction-1.0.0.xsd">
                     <registrant>
                           AUCTION-WINNER-1
                     </registrant>
                  </check>
               </extension>
               <clTRID>
                  nlr23s#2024-04-15T16:07:37.622471
               </clTRID>
         </command>
      </epp>

.. code-block:: shell
   :caption: FRED-eppic example

   > check-domain nic.cz example.cz --auction.registrant=AUCTION-WINNER-1

.. index:: ⒺchkData, Ⓔcd, Ⓔname, Ⓔreason, ⓐavail, ⓐlang


Response element structure
^^^^^^^^^^^^^^^^^^^^^^^^^^

The :ref:`response <struct-response>` from the FRED EPP server contains
the result, response data, and transaction identification.

See also :ref:`succ-fail`.

The response data element (``<resData>``) contains a single child element
``<domain:chkData>`` which declares the ``domain`` :doc:`namespace and schema </EPPReference/SchemasNamespaces/index>`
and it contains the following child elements:

* ``<domain:cd>`` **(1..n)** – the check resolution of a single domain name:

   * ``<domain:name>`` **(1)** – the domain name as :term:`eppcom:labelType`,

      * ``@avail`` **(R)** – availability as :term:`xs:boolean`;
        ``true`` – available, ``false`` – not available,

   * ``<domain:reason>`` **(0..1)** – if the availability is negative,
     this element contains an explanation why the domain name is not available,
     as :term:`fredcom:msgType`.

      * For domains in auctions, specific explanations can be displayed:
        
          * ``Auction pending`` – the domain is in auction and has no winner yet.

          * ``Only the auction winner is authorized to register this domain`` – when
            the auction winner is known and is possible to ``<domain:create>`` only by the winner.

      * ``@lang`` – language of the reason as :term:`xs:language`;
        default is ``en`` (English).

.. code-block:: xml
   :caption: Example

   <epp xmlns="urn:ietf:params:xml:ns:epp-1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:ietf:params:xml:ns:epp-1.0 epp-1.0.xsd">
   <response>
      <result code="1000">
         <msg>Command completed successfully</msg>
      </result>
      <resData>
         <domain:chkData xmlns:domain="http://www.nic.cz/xml/epp/domain-1.4" xsi:schemaLocation="http://www.nic.cz/xml/epp/domain-1.4 domain-1.4.5.xsd">
         <domain:cd>
            <domain:name avail="1">available-domain.cz</domain:name>
         </domain:cd>
         <domain:cd>
            <domain:name avail="0">registered-domain.cz</domain:name>
            <domain:reason>Registered already</domain:reason>
         </domain:cd>
         <domain:cd>
            <domain:name avail="0">auction-pending.cz</domain:name>
            <domain:reason>Auction pending</domain:reason>
         </domain:cd>
         <domain:cd>
            <domain:name avail="1">is-auction-winner.cz</domain:name>
         </domain:cd>
         <domain:cd>
            <domain:name avail="0">is-not-auction-winner.cz</domain:name>
            <domain:reason>Only the auction winner is authorized to register this domain</domain:reason>
         </domain:cd>
         </domain:chkData>
      </resData>
      <trID>
         <clTRID>nlr23s#2024-04-15T16:07:37.622471</clTRID>
         <svTRID>ReqID-0063282953</svTRID>
      </trID>
   </response>
   </epp>

.. code-block:: shell
   :caption: FRED-eppic example

   Command completed successfully (1000)

   data:
   - avail: true
   name: nic.cz
   reason: null
   - avail: false
   name: example.cz
   reason: Registered already
