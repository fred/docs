
.. index::
   pair: sendAuthInfo; contact
   triple: send; authInfo; contact

Send AuthInfo for contact
==========================

The contact's sendAuthInfo command provides the autorization information (AuthInfo)
which can be used either as a password for contact transfers, or as a parameter of the ``contact_info``
command to make hidden data visible.


The client sends only the request for the provision to the Registry and
the Registry sends the AuthInfo to the email of the contact. These actions happen even when
the transfer of the contact is forbidden, i.e the contact has the status ``serverTransferProhibited``.

This command is a part of the :doc:`protocol extension </EPPReference/ProtocolBasics/ProtocolExtensions>`
defined by the FRED EPP server.

The command must be contained in the ``<fred:sendAuthInfo>`` command type.

.. index:: Ⓔextcommand, ⒺsendAuthInfo, Ⓔid

Command element structure
-------------------------

The ``<contact:sendAuthInfo>`` element must declare the ``contact`` namespace
and :doc:`schema </EPPReference/SchemasNamespaces/index>` and it must contain the following child element:

* ``<contact:id>`` **(1)** – a contact handle as :term:`fredcom:objIDType`.

.. code-block:: xml
   :caption: Example

   <?xml version="1.0" encoding="utf-8" standalone="no"?>
   <epp xmlns="urn:ietf:params:xml:ns:epp-1.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="urn:ietf:params:xml:ns:epp-1.0 epp-1.0.xsd">
   <extension>
      <fred:extcommand xmlns:fred="http://www.nic.cz/xml/epp/fred-1.5"
       xsi:schemaLocation="http://www.nic.cz/xml/epp/fred-1.5 fred-1.5.0.xsd">
         <!-- Custom command type -->
         <fred:sendAuthInfo>
            <!-- The object-defined command -->
            <contact:sendAuthInfo xmlns:contact="http://www.nic.cz/xml/epp/contact-1.6"
             xsi:schemaLocation="http://www.nic.cz/xml/epp/contact-1.6 contact-1.6.6.xsd">
               <contact:id>CID-MYOWN</contact:id>
            </contact:sendAuthInfo>
         </fred:sendAuthInfo>
         <fred:clTRID>rhgo002#17-08-08at17:10:00</fred:clTRID>
      </fred:extcommand>
   </extension>
   </epp>

.. code-block:: shell
   :caption: FRED-client equivalent

   > sendauthinfo_contact CID-MYOWN

Response element structure
--------------------------

The FRED EPP server responds with a :ref:`plain result message <plain-result>`
which does not contain any response data (no ``<resData>``).

.. Important:: Since FRED 2.48.0, server can be configured
   to return partially disclosed email addresses
   of AuthInfo recipients. In such case, response may contain ``<resData>``.

.. code-block::
   :caption: Example - possible configuration in FRED 2.48.0

    [rifd]
    # With this flag set, even if disclose policy "hide" is set for
    # email and notify_email, their values can be partially disclosed
    # in response to SendAuthInfo.
    partially_disclose_contact_emails = true

.. code-block:: xml
   :caption: Example - possible response in FRED 2.48.0

   code
    <?xml version="1.0" encoding="UTF-8"?>
    <epp xmlns="urn:ietf:params:xml:ns:epp-1.0"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="urn:ietf:params:xml:ns:epp-1.0 epp-1.0.xsd">
        <response>
            <result code="1000">
                <msg>Command completed successfully</msg>
            </result>
            <resData>
                <contact:sendAuthInfoData
                    xmlns:contact="http://www.nic.cz/xml/epp/contact-1.6"
                    xsi:schemaLocation="http://www.nic.cz/xml/epp/contact-1.6 contact-1.6.6.xsd">
                    <contact:email>
                            a*****@b*****.*,c*****@d*****.*
                    </contact:email>
                </contact:sendAuthInfoData>
            </resData>
            <trID>
                <clTRID>somecltrid</clTRID>
                <svTRID>ReqID-0000000001</svTRID>
            </trID>
        </response>
    </epp>

See also :ref:`succ-fail`.
