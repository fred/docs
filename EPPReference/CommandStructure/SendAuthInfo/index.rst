


Send AuthInfo
===============

Commands for provision of the autorization information (AuthInfo) for object transfers.

.. toctree::

   SendAuthInfoDomain
   SendAuthInfoContact
   SendAuthInfoNsset
   SendAuthInfoKeyset
