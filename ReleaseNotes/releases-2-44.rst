============
Version 2.44
============

See repository changelogs for more details. 


Release 2.44.0
==============

.. rubric:: Enhancements

* add concept of external identity which can be linked to the registry contact
* modify domain browser backend to be usable with mojeID contacts and external identity contacts
* add new contact states for disabling the contact attributes change
   * related changes in epp contact update and contact auto merge procedure
