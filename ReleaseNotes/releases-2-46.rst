============
Version 2.46
============

See repository changelogs for more details.


Release 2.46.0
==============

.. rubric:: Enhancements

* :repo:`fred/api/registry`
   * add ``log_entry_id`` into history records of objects

* :repo:`fred/rdap`
   * use logging in :ref:`grill <arch-utilities>` (a gRPC interface logging library - :repo:`fred/utils/grill`) for RDAP



* :repo:`fred/api/logger`
   * add ``log_entry_ident`` and ``session_ident`` to message ``LogEntryInfo`` in service ``SearchLoggerHistory``

* :repo:`FERDA <fred/ferda>`

    * add new logger module for searching and viewing logs
    * add possibility of more than one reporting backend
    * add date-time picker into date-time widget in Messages
    * add pasting of date from `clipboard` into date-time widget in Messages
    * minor adjustments of report list module

.. rubric:: Bugfixes

* :repo:`fred`
   * fix build for Fendora Linux distribution

* :repo:`fred/server` and :repo:`fred/libfred`
   * fix of auto registering procedure
   * rework object factories to manual registration instead of the malfunctioning automatic merge procedure

* :repo:`fred/server`
   * fix ``101 not found`` error in the case of existing registrar after the command ``whois -h whois.nic.cz -T registrar HANDLE``
   * fix poll message ``updateData`` sending after domain/keyset/nsset change by system registrar

* :repo:`FERDA <fred/ferda>`
   * fix value of ``transfered`` and ``updated`` attributes (dates)
