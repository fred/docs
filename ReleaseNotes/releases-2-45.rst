============
Version 2.45
============

See repository changelogs for more details.


Release 2.45.0
==============

.. rubric:: Enhancements

* change Authinfo after after a successful execution of the info contact command with the AuthInfo parameter
* add possibility to send contact's Authinfo on email given in the registry even in case the transfer of the contact is forbidden, i.e the contact has the status ``serverTransferProhibited``
