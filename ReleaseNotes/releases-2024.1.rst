===============
Version v2024.1
===============

See repository changelogs for more details. 

.. _release-v2024.1:

Release v2024.1
===============

.. rubric:: Enhancements

* :repo:`fred/server`
    * Add optional parameter ``registrant`` to the ``check-domain`` EPP function which checks domain availability
      for a specific contact in the registry.
    * Added items summary to accounts invoice.
* :repo:`fred/backend/zone`
    * New zone service implementation (deprecates old pyfred genzone).

* :repo:`Documentation <fred/docs>`
    * Replace :ref:`Demo installation script <FRED-Admin-Demo>` with pre-installed virtual image.
