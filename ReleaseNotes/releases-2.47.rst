============
Version 2.47
============

.. _release-2.47:

Release 2.47.0
==============

We are going to implement an autorization information (AuthInfo) modification
with a time-limited validity.

The AuthInfo is used in the domain registry for object
transfer or for viewing object details.

The validity period of the generated AuthInfo will be set to 14 days. :term:`CZ-specific`

* New :term:`epp` schemas of the version ``2.4.3`` are available at https://www.nic.cz/page/744/registracni-system/.
	* allow to set AuthInfo as an optional attribute of the ``info_keyset``, ``info_nsset`` and ``info_domain`` :term:`epp` request.

AuthInfo without TTL (previous version)
=======================================

.. rubric:: Previous state scenarios

For object transfer to another registrar:

1. The user needs to get AuthInfo to transfer an object (see :ref:`Four options to get AuthInfo without TTL <get-authinfo-without-ttl>`).
2. The user passes the obtained AuthInfo to the target registrar.
3. The target registrar calls the ``transfer`` function.
4. After the successful transfer the registry generates a new AuthInfo.

For registrar's access to user's contact information hidden according to the disclosure settings:

1. The user requests the current AuthInfo from the registrar website or gets new AuthInfo (see :ref:`Four options to get AuthInfo without TTL <get-authinfo-without-ttl>`).
2. The user passes obtained AuthInfo to the registrar to whom he wishes to disclose his non-public data on a one-time basis.
3. After the data is disclosed the registry generates a new AuthInfo.

.. _get-authinfo-without-ttl:
.. rubric:: Four options to get AuthInfo without TTL

The user can get AuthInfo:

1. via the designated registrar's website of the transferred object or the contact connected to the object,
    * On the designated registrar's website the user gets the current AuthInfo or sets his own AuthInfo, i.e.:
        * The designated registrar calls a ``send_authinfo`` function, then the registry sends the current AuthInfo
				  by e-mail to all contacts relevant for the object, one of them should be also a transfer initiator (the user).
        * The designated registrar uses an ``info`` function and passes the current AuthInfo returned by the registry the user.
        * The designated registrar sets up AuthInfo value in the registry via an ``update`` function.
2. via the target registrar's website,
    * The target registrar calls ``send_authinfo`` function, then the registry sends the current AuthInfo
		  by e-mail to all contacts relevant for the object, one of them should ale be the transfer initiator (the user).
3. by viewing the current AuthInfo in the Domain browser,
4. via the request form directly on the CZ.NIC Association's website. :term:`CZ-specific`

AuthInfo with TTL (current version)
===================================

.. rubric:: Current state scenarios

For object transfer to another registrar:

1. The user needs to get AuthInfo for object transfer (see :ref:`Four options to get AuthInfo with TTL <get-authinfo-with-ttl>`).
2. The user passes the obtained AuthInfo to the target registrar.
3. The target registrar calls a ``transfer`` function.
4. The registry invalidates the used AuthInfo.

For registrar's access to user's contact information hidden according to the disclosure settings:

1. The user requests to generate and send new AuthInfo from the registrar's website or gets new AuthInfo (see :ref:`Four options to get AuthInfo with TTL <get-authinfo-with-ttl>`).
2. The user passes obtained AuthInfo to the registrar to whom he wishes to disclose his non-public data on a one-time basis.
3. AuthInfo is valid even after the registrar obtains the non-public data. AuthInfo validity is determined by the TTL, limit starts after the AuthInfo is generated.

.. _get-authinfo-with-ttl:
.. rubric:: Four options to get AuthInfo with TTL

The user can get Authinfo:

1. via the designated registrar's website of the transferred object or the contact connected to the object,
    * On the designated registrar's website the user requests valid AuthInfo or sets his own AuthInfo, i.e.:
        * The designated registrar calls a ``send_authinfo`` function, then the registry generates and sends the AuthInfo
				  by e-mail to all contacts relevant for the object. One of them should also be the transfer initiator (the user).
        * The designated registrar sets up AuthInfo value to the registry via an ``update`` function.
2. via target registrar website,
    * Target registrar calls a ``send_authinfo`` function, then the registry generates and sends the AuthInfo
		  by e-mail to all contacts relevant for the object. One of them should be also transfer initiator.
3. sets his own AuthInfo in the Domain browser,
    * This AuthInfo is valid only for MojeID contact currently logged in. However, it is possible to use it for transfer of linked objects.
    * The system saves AuthInfo with corresponding TTL only if it meets password stregth requirements.
4. via the request form directly on the CZ.NIC Association website. :term:`CZ-specific`

.. rubric:: New option for AuthInfo validity verification

It will not be possible to get an AuthInfo value of any object in the registry via ``info`` function.
The registry will allow an AuthInfo verification by registrar in a different way.
If any registar sends the correct AuthInfo in any object's ```info`` command, the registry returns a valid data about the requested object. If the AuthInfo is not correct
(all are already expired or none is generated/saved), the registry returns error ``2202 - Invalid authorization information``.
