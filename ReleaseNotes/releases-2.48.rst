============
Version 2.48
============

See repository changelogs for more details.

.. _release-2.48:

Release 2.48.0
==============

.. rubric:: Enhancements

* :repo:`fred/server`
    * Transition from CORBA to :ref:`gRPC logger <FRED-Arch-servers-logger>`
    * :ref:`TechCheck <FRED-Arch-servers-techcheck>` removed
    * Old :ref:`verifications <FRED-Admin-AdminTasks-Objects-contact-verification>` removed, only `automatic` verification remains
    * Enhance AuthInfo with TTL implementation:
            * Objects cannot be created if the `create` command contains AuthInfo
            * Registrars can see a hint to which email was an AuthInfo sent (e.g. `a*****@b*****.**`)
            * In the EPP `update` operation the server now accepts only AuthInfo with minimum number of characters (can be configured)
            * In the EPP `update` operation, entering the AuthInfo as an empty string invalidates the current AuthInfo value
    * Change registrar scoring system from 0-5 to 0-100%
    * New contact state flag `serverLinkProhibited` – disallows linking of new objects to the object with this flag
    * Configurable logger name for LoggerClient
    * Add new `clean_expired_authinfos` command in `fred-admin`
    * Add possibility to end domain name life cycle with auction (configurable per zone)
    * fred-mifd now uses :ref:`messenger <FRED-Arch-servers-messenger>` to send emails/letters/SMS
    * MojeID registry backend
            * The beginning of registry and MojeID contact relationship transformation
                * Implement new methods to create and update contact
                * Support for optional permanent address
                * No handling of the verification states – will be handled by client side
                * Removal of two-phase commit for new methods

* :repo:`FRED gRPC backend services <fred/backend>`
    * :repo:`fred/backend/identity`
        * Add support for AuthInfo with TTL feature
    * :repo:`fred/backend/logger`
        * Integrate common diagnostics service using `libdiagnostics`
        * Adapt to `libstrong` library
        * Change `LogEntryInfo` (add `log_entry_id` and `session_id`)
    * :repo:`fred/backend/notify`
        * `fred-notify-contact-data-reminder` now does not send reminder to validated contacts, validation age limit can be configured
        * Switch to new storage of additional contacts
    * :repo:`fred/backend/public-request`
        * New public requests management server based on gRPC API
    * :repo:`fred/backend/registry`
        * Implement methods for registrars management
        * Add support for registrars management
        * Implement method for a host name of domain name server checking
        * Use `registrar.is_internal` flag
        * Add `validation_expires_at` into DomainInfoReply
        * Implement `get_domain_life_cycle_stage` method for WHOIS/RDAP
        * Add objects light infos
        * `get_domains_by_contact` optionally also returns deleted domains
        * Change example configuration to jinja2 template
        * Implement method `update_contact_state` (Contact service)
        * Implement ContactRepresentative service
        * Add support for domain auctions
    * FRED verifications
        * New gRPC service for validation and verification of registry contacts' data

* :repo:`FERDA <fred/ferda>`
    * Change PluginConfig from dataclass to pydantic model (the interface stayed the same, but types and values are now strictly checked)
    * Add registrar, groups and certification management
    * Add deleted domains to related domains list in contact detail
    * Add new universal REST API for registry search
    * Add plugin permissions
    * Add `output_properties` to `ferda.tests.utils.LoggerMixin.assertLog`
    * Add search by notify_email to contact detail
    * Add registry search CSV export for exact matches
    * Add action buttons to registry search results
    * Add missing object state descriptions
    * Add endpoints for contact representative
    * Enable frontend report list filtering
    * Add settings API (will replace django-settings and ferda-version context processors in the future)
    * Add URL prefixes to settings API
    * Add user API (will replace user-profile and user-permissions context processors in the future)
    * Add regal-based API to replace `ferda.backend.client`
    * Add pluggy plugin framework
    * Add create permissions plugin hook
    * Add endpoint to download certification file
    * Add a link from object detail to logger module
    * Change default search registry object type from contact to domain
    * Add message subtype labels to message detail
    * Display raw message data in message detail
    * Update log request detail design
    * Remove opening links in new tabs
    * Add `registrant_ref` to domain info results
    * Register logger object reference types
    * Limit minimal length of contact registry search queries

* FERDA plugins
    * FERDA verification – new plugin for manual contact verification
    * FERDA public request – new plugin for public request management
    * FERDA auction – new plugin for domain auction management

* Messaging: :repo:`fred/backend/messenger`, :repo:`fred/backend/fileman` and :repo:`fred/django-secretary`
    * New services for all parts of messaging process
    * Migrate all messaging to the new services
    * See :ref:`messenger <FRED-Arch-servers-messenger>`

* :repo:`Documentation <fred/docs>`
    * :ref:`Demo installation script <FRED-Admin-Demo>`

.. rubric:: Bugfixes

* Fix registrar EPP auth operations in libfred (bump dependencies)
* Fix registrar group membership
* Fix three switches which malfunctioned under certain circumstances (`fred-admin --invoice_export --invoice_dont_send`,
  `--invoice_archive --invoice_dont_send`, `--invoice_archive --debug_context`)
* Fix `serverUpdateProhibited` block
* Fix single invoice export
* poll-client now uses UTC timestamp when calling logger (fix for new logger)
* fred-logd `getRequestCountUsers` now expects UTC timestamps
* Set contact validation public request as processed even if email is not sent
