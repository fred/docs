============
Version 2.42
============

See repository changelogs for more details. 

Release 2.42.0
==============

.. rubric:: Enhancements

* :repo:`FERDA <fred/ferda>`
   - new project for modern web administration interface (future Daphne replacement)
   - implemented search and detail browser for basic registry objects (domain, contact, nsset, keyset, registrar) including history
   - implemented user request and data access logging
   - implemented possibility to add additional contact representative to registry contact
   - support for FIDO2 authentication

* :repo:`Domain life-cycle parameters <fred/server>` can be now changed over time (with parameters history being maintained).
   - ``fred-admin --domain_lifecycle_parameters``
       - new interface for domain life-cycle parameters management
       - replacement for domain parameters originally set with ``--enum_parameter_change``


.. rubric:: Bugfixes

* :repo:`fred/server`
   - ``fred-admin --block_registrar_over_limit`` -- fix daily e-mail notification (query time zone)

* :repo:`fred/server`
   - ``fred-admin --process_public_requests`` -- fix e-mail notification after resolving block/unblock requests
