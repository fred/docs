============
Version 2.49
============

See repository changelogs for more details.

.. _release-2.49:

Release 2.49.0
==============

Added support for the BSA – GlobalBlock feature.
