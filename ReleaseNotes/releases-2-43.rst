============
Version 2.43
============

See repository changelogs for more details.


Release 2.43.0
==============

.. rubric:: Enhancements

* :term:`epp` command ``info_contact`` has new optional attribute AuthInfo to allow other than :term:`designated registrar` request disclosed attributes of a given contact
* :term:`epp` command ``info_contact`` shows:
   * data according to the set ``disclose flags``, if a given registrar **is not** a :term:`designated registrar` for ``<handle>``
   * all data,
   	* if a given registrar **is** a :term:`designated registrar` for ``<handle>``
   	* if a given registrar **is not** a :term:`designated registrar` for ``<handle>``
   		* but enters AuthInfo in the request of a given contact
   		* but contact with ``<handle>`` **is** attached to an object like domain (in the role of ``registrant`` or ``admin``), for which a given registrar **is** a :term:`designated registrar`

* if the server evaluates AuthInfo in the ``info_contact`` request as incorrect, it returns error ``2202 - Invalid authorization information``
* :term:`epp` command poll req for a contact data change message, displays the same information as before

* all data:
	* receive a :term:`designated registrar` of a given contact
	* receive :term:`designated registrars<designated registrar>` of a domain, which has the given contact established in the role of ``registrant`` or ``admin``

* New :term:`epp` schemas of the version ``2.4.2`` are available at https://www.nic.cz/page/744/registracni-system/
	* allow to set AuthInfo as an optional attribute of the ``info_contact`` :term:`epp` request
	* add new contact states to inform that some of the contact attributes are locked and cannot be changed
