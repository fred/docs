.. _FRED-Features-GlobalBlock:

GlobalBlock
================

GlobalBlock is a unified domain name blocking services created by the Brand Safety Alliance (BSA).
It is used for blocking domain registration across many TLDs.

Prerequisites
--------------

Before activating GlobalBlock extension, you need to have a valid contract with the BSA.

GlobalBlock extension is available only to users with paid `FRED support <https://fred.nic.cz/en/support/>`_.
If you are interested, please contact us at fred@nic.cz.

Before installing GlobalBlock, make sure you have the following packages in the correct
(or newer) versions installed.

.. list-table:: APP packages
   :header-rows: 1
   :widths: 20, 20
   :align: left

   * - Package
     - Version
   * - ``fred-idl``
     - 2.43.0
   * - ``fred-backend-registry``
     - 2.18.0
   * - ``fred-bsapp``
     - 0.1.0
   * - ``fred-pifd``
     - 2.64.0
   * - ``fred-rifd``
     - 2.64.0
   * - ``fred-adifd``
     - 2.64.0
   * - ``fred-mifd``
     - 2.64.0
   * - ``fred-dbifd``
     - 2.64.0
   * - ``fred-accifd``
     - 2.64.0
   * - ``fred-akmd``
     - 2.64.0
   * - ``fred-common``
     - 2.64.0

.. list-table:: WHOIS packages
   :header-rows: 1
   :widths: 20, 20
   :align: left

   * - Package
     - Version
   * - ``libapache2-mod-whoisd``
     - 3.15.0


Installation steps
------------------

This section contains specific steps for installing GlobalBlock extension to the FRED system.

.. Important:: To install GlobalBlock, you need the ``fred-bsapp`` package. This package is not publicly available,
   we send it only upon request. Please, contact us at fred-support@nic.cz.


Databases
^^^^^^^^^

#. Create ``bsapp`` schema – user ``postgres``, database ``fred``

   .. code-block:: sql

        -- Create roles
        CREATE ROLE bsapp_ro NOLOGIN;
        CREATE ROLE bsapp_rw NOLOGIN;
        CREATE USER bsapp WITH PASSWORD '...' IN ROLE bsapp_rw;
        CREATE USER bsapp_view WITH PASSWORD '...' IN ROLE bsapp_ro;

        -- Create schema
        CREATE SCHEMA bsapp AUTHORIZATION bsapp_rw;
        ALTER USER bsapp SET search_path = 'bsapp';
        ALTER USER bsapp_view SET search_path = 'bsapp';

        -- Create priviledges
        REVOKE ALL ON SCHEMA public FROM bsapp_ro;
        REVOKE ALL ON SCHEMA public FROM bsapp_rw;
        -- It's needed to provide privileges to schema *and* tables.
        GRANT ALL ON SCHEMA bsapp TO bsapp_rw;
        GRANT ALL ON ALL TABLES IN SCHEMA bsapp TO bsapp_rw;
        GRANT USAGE ON SCHEMA bsapp TO bsapp_ro;
        GRANT SELECT ON ALL TABLES IN SCHEMA bsapp TO bsapp_ro;

#. Setup ``bsapp`` schema priviledges – user ``bsapp``, database ``fred``

   .. code-block:: sql

        -- Create default priviledges
        -- Must be run as 'bsapp' user!
        ALTER DEFAULT PRIVILEGES IN SCHEMA bsapp GRANT ALL ON TABLES TO bsapp_rw;
        ALTER DEFAULT PRIVILEGES IN SCHEMA bsapp GRANT SELECT ON TABLES TO bsapp_ro;

#. Run ``bsapp`` migrations

   .. code-block:: shell

        #> app
        export ALEMBIC_CONFIG=/opt/venvs/fred-bsapp/lib/python3.8/site-packages/bsapp/alembic.ini
        echo "Migrations started"
        echo "Alembic config: $ALEMBIC_CONFIG"
        alembic history -i
        echo "Running migrations..."
        alembic upgrade head
        alembic history -i
        echo "Migrations completed"

#. Verify, that your FRED data model is in version ``2.58.0`` or newer

   .. code-block:: sql

        SELECT val FROM enum_parameters WHERE name = 'model_version';
          val   
        --------
        2.58.0


Configuration
^^^^^^^^^^^^^

#. ``app@/etc/fred/bsapp.conf``

   .. code-block:: yaml

        api_key: THE_API_KEY
        # TEST values:
        # api_key: API_KEY_VALUE
        # api_url: https://api-ote.bsagateway.co/

        # See https://docs.sqlalchemy.org/en/20/core/engines.html#database-urls for details
        db_connection: postgresql+psycopg://USER:PASS@:6432/fred?host=/var/run/postgresql

        # Disable pooling in client, if using pgbouncer.
        db_poolclass: sqlalchemy.pool.NullPool

        db_schema: bsapp
        logging:
            version: 1
            disable_existing_loggers: False
            formatters:
                verbose:
                    format: '%(asctime)s %(levelname)-8s [%(process)d:%(thread)d] %(name)s:%(funcName)s:%(lineno)s %(message)s'
            handlers:
                syslog:
                    class: logging.handlers.SysLogHandler
                    formatter: verbose
                    address: '/dev/log'
            loggers:
                '':
                    handlers: [syslog]
                    level: DEBUG
        registry_netloc: localhost:2240

#. Whois apache configuration:

   Add the following line:

   .. code-block:: shell

        WhoisBlacklistMessage "BSAPP" "% This name has been blocked by a GlobalBlock service."

   to virtualhost section of ``/etc/apache2/sites-available/whois.nic.cz.conf`` so it will look like this:

   .. code-block:: shell

        <VirtualHost *:43>
        CorbaEnable           On
        CorbaNameservice      "{{ corba.host }}:{{ corba.port }}"
        CorbaObject           "Whois"         "Whois_alias"
        CorbaObject           "LoggerNew"     "Logger_alias"
        WhoisLogdObject       "Logger_alias"
        WhoisProtocol         On
        WhoisDisclaimer       "/etc/fred/disclaimer.txt"
        WhoisObject           "Whois_alias"
        WhoisBlacklistMessage "BSAPP" "% This name has been blocked by a GlobalBlock service."
        ...


Cron jobs
^^^^^^^^^

- **Cron job description:** Fetch new orders from BSA
   - **Server:** ``app``
   - **When / Recurrence:** hourly
   - **Command:** ``fred-bsapp-fetch-orders``
- **Cron job description:** Process new orders from BSA
   - **Server:** ``app``
   - **When / Recurrence:** hourly (offset from fetch by 20 mins)
   - **Command:** ``fred-bsapp-process-orders``
- **Cron job description:** Report unregistrable domains to BSA
   - **Server:** ``app``
   - **When / Recurrence:** daily (try to avoid parallel runs with other crons)
   - **Command:** ``fred-bsapp-report-domains [ZONE]...``
- **Cron job description:** Daily check of blocked domains
   - **Server:** ``app``
   - **When / Recurrence:** daily (try to avoid parallel runs with other crons)
   - **Command:** ``fred-bsapp-check-domains``
