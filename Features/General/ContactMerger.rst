


Contact merger
--------------

There is a method to minimize duplicate contact records in the Registry
automatically. However, only contacts which are identical and mergeable
by strict rules, are merged.

:doc:`More about the contact merger. </Concepts/ContactMerger>`

.. NOTE manual merge in Domain Browser extension or through CLI
