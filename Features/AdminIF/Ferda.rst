.. _Ferda-Features:

FERDA webadmin features
-----------------------

See also :doc:`/Architecture/TopLevelComponents/Clients`.

General
^^^^^^^

* Two basic languages (CS, EN)
* Two-factor user authentication via FIDO2 tokens (optional)
* User permissions
* Referenced objects are linked by hypertext (object handles, attached files)
* Logging via gRPC
* Registry module
      * Viewing object details
      * Viewing registrar details
      * Basic or detailed view
      * Comparison of two points in history side by side
      * Search in recent data
      * Search in historic data
* Messages module (all e-mails, sms and letters in one place)
* Report module
* Logger module for searching and viewing logs
* List of domains linked to one contact


Configurables
~~~~~~~~~~~~~

* Django and apps settings
* State flags grouping
* State flags descriptions

In development
^^^^^^^^^^^^^^

* Completely new contact verification process with representatives for non-EU countries
* Attaching files to objects
* Full history summary downloadable as a PDF file


Reports module
^^^^^^^^^^^^^^

The reports module is a feature that provides a front-end for repeatable registry queries 
(referred to as **reports** for the rest of this section), which are defined via an :ref:`administration interface<Django admin interface>`.
Selecting a report triggers the defined database query and the output is displayed in the browser.

.. _Django admin interface:

Django administration interface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Django interface allows you to :ref:`manage user permissions<Access control>` to run or modify reports and sync them with the database.

The interface is accessible either from FERDA (select *Admin* in the user menu in the header), or at the path
of your FERDA installation configured during deployment (default is `/admin`, e.g. `ferda.example.com/admin`).

Only users with **Staff** or **Superuser** status can log into the administration interface.

Reports section
"""""""""""""""

In this section, you can synchronize reports with the database, modify their permissions and labels,
add new reports or modify existing ones using SQL.

The **Manage reports** button allows the user to input an SQL function that creates or modifies reports.

Synchronizing reports
"""""""""""""""""""""

To perform synchronization of reports from individual backends with the main FERDA database, use the **Sync reports**
button in the :ref:`Django admin interface` to open the synchronization dialog.

Modifying reports
"""""""""""""""""

When you select a report, you may change or view the following:

* :ref:`Object permissions<Access control>` (which users and groups can run the report).
* History (view history of report changes).
* Label of the report, its long description, and labels of the input and output parameters.

Creating or modifying report SQL definitions
""""""""""""""""""""""""""""""""""""""""""""

To create or modify a report:

#. Go to the Django administration interface.
#. Select **Reports**.
#. Select **Manage reports**.
#. Select the service your report will belong to.
#. Input the SQL function that adds or modifies your report.

When modifying a report this way, the function name and parameters must be identical to the already existing function.

Only users with :ref:`Can add Report<Access control>` can create reports.

Example 1: SQL to add a report
""""""""""""""""""""""""""""""

The following SQL creates a report that returns a list of domains for a specified holder country code.

.. code-block:: sql

      CREATE OR REPLACE FUNCTION domain_list_by_holder_country(country_code VARCHAR)
      RETURNS TABLE (
                        domain VARCHAR,
                        sponsoring_registrar VARCHAR,
                        holder VARCHAR,
                        crdate TIMESTAMP,
                        exdate DATE,
                        zone_status VARCHAR,
                        state_flags TEXT
                  )
      AS
      $$
      SELECT d.domain,
            d.sponsoring_registrar,
            d.holder,
            d.crdate,
            d.exdate,
            CASE WHEN d.state_flags @> '{outzone}' THEN 'OUT' ELSE 'IN' END AS zone_status,
            ARRAY_TO_STRING(d.state_flags, ', ') AS state_flags
            FROM (
                  SELECT doreg.name AS domain,
                        r.handle AS sponsoring_registrar,
                        coreg.name AS holder,
                        doreg.crdate,
                        d.exdate,
                        ARRAY_AGG(deos.name ORDER BY deos.importance) AS state_flags
                  FROM public.enum_country ec
                  JOIN public.contact c ON c.country = ec.id
                  JOIN public.object_registry coreg ON coreg.id = c.id
                  JOIN public.domain d ON d.registrant = c.id
                  JOIN public.object dobj ON dobj.id = d.id
                  JOIN public.registrar r ON r.id = dobj.clid
                  JOIN public.object_registry doreg ON doreg.id = d.id
                  LEFT JOIN public.object_state dos ON dos.object_id = d.id AND dos.valid_to IS NULL
                  LEFT JOIN public.enum_object_states deos ON deos.id = dos.state_id
                  WHERE ec.id = UPPER(country_code)
                  GROUP BY 1, 2, 3, 4, 5
            ) AS d;
      $$
      LANGUAGE SQL
      STABLE
      SECURITY DEFINER;


Configuring reports in the registry
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To start using reports in FERDA, take the following steps:

#. Configure the database to provide required data in a controlled manner.
#. Configure registry backend to provide a gRPC endpoint for queries from FERDA.
   This is done by the ``fred-dbreport-services`` binary provided by the ``fred-backend-dbreport\`` package.
#. Configure FERDA to send report queries to the configured endpoint.

Configuring database for reports
""""""""""""""""""""""""""""""""

Reports can be configured to run directly on registry database, however, as an additional step, we recommend to encapsulate
reports within their own scheme and set up read-only user for access on target database.

Configuring registry backend for reports
""""""""""""""""""""""""""""""""""""""""

The `fred-backend-dbreport` package is used to provide the gRPC endpoint for FERDA via `/usr/sbin/fred-dbreport-services` binary.
By default, the package installs a systemd unit configuration, which creates an instance of `fred-dbreport-services` for each
configuration file that matches the following pattern: `/etc/fred/fred-dbreport-[INSTANCE_NAME].conf`.
However you can configure your own customised systemd unit as well. An example configuration file can be found in
`FRED GitLab repository <https://gitlab.nic.cz/fred/backend/dbreport/-/blob/master/example.conf>`_, or you can pass config
as parameters directly to the binary (see `--help`).

We recommend to set a new service for each database.

Installing the `fred-backend-dbreport` should configure the basic services for you. (`fredlog`, `messenger` and `registry`).

Configuring reports in FERDA
""""""""""""""""""""""""""""

To know which endpoint to call when running reports, FERDA uses `FERDA_REPORTS` system variable.
See the `README <https://gitlab.nic.cz/fred/ferda/-/blob/master/README.rst>`_ file. If you run FERDA on docker,
to simplify the configuration, you can use the `.env file <https://gitlab.nic.cz/fred/ferda/-/blob/master/docker/example.env>`_
to directly set the configuration values for each service.
These will be applied during `setup <https://gitlab.nic.cz/fred/ferda/-/blob/master/docker/build/ferda_cfg/settings.py?ref_type=heads#L64>`_.

.. _Access control:

Controlling user access to reports
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section describes permissions that control what particular users can do with reports.

The relevant permissions in the **Users** section are marked with the `ferda | Report |` prefix and are as follows:

* Can add Report
* Can change Report
* Can delete Report
* Run report
* Can view Report

Only users with the :doc:`permission to change users </AdminManual/AdministrativeTasks/UsersPermissions>` can assign permissions.
