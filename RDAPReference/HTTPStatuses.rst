
HTTP Statuses
-------------


In RDAP, each response to a request made to the server contains appropriate data in JSON format (if applicable) together with one of the four following HTTP statuses:

* **200 OK** – Valid query for an existing object in the registry.
* **400 Bad Request** – Invalid query.
* **404 Not Found** – Valid query for a non-existent object in the registry.
* **500 Internal Server Error** – Received in case of a server error.
