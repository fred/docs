
.. index::
   pair: transfer; process

Object transfer
===============

Object transfer is a mechanism that allows to change the :term:`designated registrar`
of an object.

Each object has **authorization information** (AuthInfo for short) that in this case
represents a transfer password. The AuthInfo must be provided with the transfer request
to authorize the transfer. More information about AuthInfo TTL can be found at :ref:`config-authinfo-ttl`.

The transfer process in the FRED is different from the standard one
(see :rfc:`5730#section-2.9.3.4`) and it works as follows:

#. A contact linked to an object intended for transfer requests AuthInfo of the object.
   He can do so via the current registrar, via the new registrar or
   by submitting a \ :term:`public request` to the Registry directly.
#. The AuthInfo is provided to the contact either through one of the involved
   registrars, or sent from the Registry.
#. The contact requests the transfer from the new registrar and provides
   the AuthInfo.
#. The new registrar requests the transfer from the Registry (via EPP) and
   provides the AuthInfo.
#. The Registry transfers the object immediately and generates new AuthInfo
   for the transferred object.
#. The Registry notifies the old registrar and the linked contacts about the transfer.

This model favours the holder because it does not allow
the current designated registrar to reject nor inhibit the transfer.

.. NOTE Public request and Domain browser provide direct access to AuthInfo
   without involving a registrar

.. _fig-features-transfer:

.. figure:: _graphics/Transfer.png
   :alt: Object transfer process sequence diagram
   :align: center

   Sequence diagram – Object transfer process

Further authorization options
-----------------------------

A transfer of a domain may also be authorized using AuthInfo of the domain holder
or any of its administrative contacts.

A transfer of an nsset or keyset may also be authorized using AuthInfo of any
of its technical contacts.
