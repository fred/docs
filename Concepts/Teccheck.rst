


DNScheck (Technical checks)
===========================

DNScheck is a system for checking nssets and DNS records.
As a result, it monitors the quality of domain delegation.
The system periodically scans all nssets in the registry and runs
tests on them using Zonemaster.

DNScheck is a replacement for the old technical checks.

Prerequisites:

* FRED system
* Messenger
* `Zonemaster software package <https://github.com/zonemaster/zonemaster/>`_

DNScheck is a dockerized Python service with its own database (PostgreSQL).
The service selects which nssets will be checked and tells the Zonemaster
service to run the DNS records and delegation test of associated domains.
Then it collects the results, aggregates them and, if there are any problems found,
sends notifications to nsset technical contacts.

Zonemaster
----------

Zonemaster is a third-party open-source service that runs the DNS records and domain delegation tests.
Each test ends with a severity level evaluation which is then processed by DNScheck.

Number of report for each severity is calculated for the domains delegation of each nsset.

Severity levels (as described in the Zonemaster documentation):

* ``CRITICAL`` – The message means a very serious error.
* ``ERROR`` – The message means a problem that is very likely (or possibly certain) to negatively affect
  the function of the zone being tested, but not so severe that the entire zone becomes unresolvable.
* ``WARNING`` – The message means something that will under some circumstances be a problem,
  but that is unlikely to be noticed by a casual user.
* ``NOTICE`` – The message means something that should be known by the zone's administrator
  but that need not necessarily be a problem at all.
* ``INFO`` – The message is something that may be of interest to the zone's administrator
  but that definitely does not indicate a problem.

For details, please visit the `Zonemaster documentation <https://doc.zonemaster.net/latest/>`_.

.. Note:: The tests are only informative,
   they do not affect inclusion/exclusion of a domain in/from a zone.

Reporting
---------

You can set a level of reporting (``reportlevel``) for each nsset.
The level defines at which severity are test result notifications sent.

.. list-table:: Reporting levels
   :header-rows: 1
   :widths: 10, 15

   * - ``reportlevel``
     - Send notification according to severity
   * - 0
     - never
   * - 1
     - ``CRITICAL``
   * - 2
     - ``CRITICAL``, ``ERROR``
   * - 3
     - ``CRITICAL``, ``ERROR``, ``WARNING``
   * - 4
     - ``CRITICAL``, ``ERROR``, ``WARNING``, ``NOTIFY``

If during the evaluation at least one test on at least one associated domain ends
with the given severity, a notification e-mail is sent. The email contains a count
of all problems found with reported severity and a list of the most
problematic domains (ordered from worst results). A notification e-mail is sent
to all nsset technical contacts.
