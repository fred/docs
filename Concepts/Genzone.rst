


Zone generation
===============

This chapter provides an overview of zone generation functionality in FRED.

FRED can generate zone files, which are compliant with
:rfc:`1034`, :rfc:`1035`, :rfc:`2308`, :rfc:`4027`.

These zone files contain SOA, NS, A, AAAA and DS records, which are consistent
with the data in the registry. The domain records are generated based on domain, 
nsset and keyset objects in the registry. FRED generates domains into the zone
only if they are linked to a nsset. Domains can also be kept administratively
out of the zone by statuses. DNSKEY records are immediately transformed into 
DS records and both A and AAAA records are only generated when GLUE records 
are required.

FRED itself only generates the text representation of zone file. The actual 
authoritative domain name service must be done through a DNS server such as 
KNOT or BIND. Although zone generator can execute additional scripts after zone
file is generated in order to automate validation.
