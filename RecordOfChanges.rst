=================
Record of Changes
=================

Overview of changes in documentation from previous editions.
For changes in software see :doc:`/ReleaseNotes/index`.

.. tabularcolumns:: |p{0.075\textwidth}|p{0.075\textwidth}|p{0.25\textwidth}|p{0.575\textwidth}|

.. list-table::
   :header-rows: 1
   :widths: 8, 8, 24, 60

   * - Version
     - Edition
     - Segment
     - Change description
   * - **v2024.1**
     - **1.4**
     - :doc:`/AdminManual/Installation/InstallUbuntu`
     - Update installation manual (add individual nodes)
   * -
     -
     - :doc:`/AdminManual/Installation/FredDemo`
     - Update demo image from VirtualBox to ``.qcow2`` format
   * -
     -
     - :doc:`Concepts/LifeCycle/Domains`
     - Add auctions to domain registration expiration schema
   * -
     - **1.3**
     - :doc:`Concepts/AKM`
     - Add section Scan results
   * -
     -
     - :doc:`Concepts/Teccheck`
     - Replace old Technical checks with DNScheck
   * -
     - **1.2**
     - :doc:`/AdminManual/Installation/InstallUbuntu` |BR|
       :doc:`/AdminManual/Installation/FredDemo`
     - Separate installation manual to demo and production
   * - 
     - **1.1**
     - :doc:`/AdminManual/Installation/InstallUbuntu`
     - Update demo installation manual to new process using virtual image
   * -
     -
     - :doc:`/ReleaseNotes/index`
     - Add release notes for FRED :ref:`release-v2024.1`
   * -
     - **1.0**
     - :doc:`/Concepts/EPPClientWorkflow`
     - Replace old ``fred-client`` with new ``eppic``
   * -
     -
     - :doc:`/Architecture/Deployment`
     - Update all packages needed for deployment
   * -
     -
     - Whole documentation
     - Other minor changes according to FRED ``v2024.1``
   * -
     -
     - :doc:`/Concepts/Genzone`
     - Add concept level info about zone generation
   * - **2.50**
     - **1.3**
     - :doc:`Concepts/AKM` |BR|
       :doc:`EPPReference/ManagedObjects/Keysets` |BR|
       :doc:`AdminManual/PeriodicTasks`
     - Changes due to new AKM version
   * -
     -
     - :doc:`Architecture/TopLevelComponents/index`
     - Update FRED component schema
   * -
     - **1.2**
     - :doc:`Features/AdminIF/Ferda`
     - Add Reports section
   * -
     - **1.1**
     - :doc:`/EPPReference/CommandStructure/Check/CheckDomain`
     - Separate ``<domain:check>`` command to non-auction and auction chapters
   * -
     - **1.0**
     - :doc:`/EPPReference/CommandStructure/Check/CheckDomain`
     - Extended command ``<domain:check>`` supporting domain auctions
   * - **2.49**
     - **1.0**
     - :doc:`Features/GlobalBlock/index`
     - Added installation manual for BSA – GlobalBlock feature
   * - **2.48**
     - **1.0**
     - Whole documentation
     - Changes according to FRED 2.48.0
   * - **2.47**
     - **1.4**
     - :doc:`/AdminManual/Installation/InstallUbuntu`
     - Updated demo installation script
   * - 
     - **1.3**
     - `Ferda Administration Manual`
     - Whole manual dissolved and incorporated into Administration Manual, Architecture and Features
   * -
     -
     - :doc:`/AdminManual/index`
     - Incorporated information from Ferda Administration Manual
   * -
     -
     - :doc:`/AdminManual/RegistryInitialization` |BR|
       :doc:`/Concepts/EPPClientWorkflow` |BR|
       :doc:`/EPPReference/CommandStructure/Info/InfoContact` |BR|
       :doc:`/EPPReference/CommandStructure/Poll/MessageTypes`
     - Changed all `REG-FRED_X` to `REG-FRED-X`
   * -
     - **1.2**
     - :doc:`/AdminManual/Installation/index` |BR|
       :doc:`/AdminManual/Appendixes/DependenciesUbuntu`
     - Discontinued support for Ubuntu 16.04, update to Ubuntu 20.04
   * -
     - **1.1**
     - :doc:`/AdminManual/Installation/BinsUbuntu`
     - Updated installation script link
   * -
     -
     - :doc:`/AdminManual/Customization/WebwhoisTemplates`
     - Small changes in `server_exception`
   * -
     - **1.0**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for FRED :ref:`release-2.47`
   * -
     -
     - :doc:`/AdminManual/Configuration` |BR|
       :doc:`/Concepts/EPPClientWorkflow` |BR|
       :doc:`/Concepts/Transfer` |BR|
       :doc:`/EPPReference/CommandStructure/Info/index` (domain, contact, nsset, keyset) |BR|
       :doc:`/EPPReference/CommandStructure/Create/index` (domain, contact, nsset, keyset) |BR|
       :doc:`/EPPReference/ManagedObjects/Common` |BR|
     - Added AuthInfo TTL information |BR|
   * - **2.46**
     - **1.0**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for FRED 2.46.0
   * -
     -
     - :doc:`Features/AdminIF/Ferda`
     - New features of FERDA webadmin |BR|
   * - **2.45**
     - **1.0**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for FRED 2.45.0
   * -
     -
     - :doc:`EPPReference/CommandStructure/SendAuthInfo/SendAuthInfoContact` |BR|
       :doc:`EPPReference/CommandStructure/SendAuthInfo/SendAuthInfoDomain` |BR|
       :doc:`EPPReference/CommandStructure/SendAuthInfo/SendAuthInfoNsset` |BR|
       :doc:`EPPReference/CommandStructure/SendAuthInfo/SendAuthInfoKeyset` |BR|
     - Added AuthInfo sending changes |BR|
   * -
     -
     - :doc:`EPPReference/CommandStructure/Info/InfoContact` |BR|
     - Changes for AuthInfo generation |BR|
   * - **2.44**
     - **1.0**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for FRED 2.44.0
   * -
     -
     - :doc:`EPPReference/ManagedObjects/Contacts` |BR|
       :doc:`EPPReference/CommandStructure/Info/InfoContact` |BR|
     - Added new contact attribute states |BR|
   * - **2.43**
     - **1.0**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for FRED 2.43.0
   * -
     -
     - :doc:`AdminManual/Configuration` |BR|
       :doc:`AdminManual/ConfigDisclosure` |BR|
     - Updated GDPR-compliant configuration |BR|
   * -
     -
     - :doc:`EPPReference/ManagedObjects/Contacts` |BR|
     - Updated Namespace and Schema |BR|
   * -
     -
     - :doc:`EPPReference/CommandStructure/Info/InfoContact` |BR|
     - New `info_contact` attribute `authInfo`  |BR|
   * - **2.42**
     - **1.0.2**
     - :doc:`EPPReference/ProtocolBasics/ServiceDiscovery`
     - Fix level of `expiry` item
   * -
     - **1.0.1**
     - Whole documentation
     - Added new FRED logo
   * -
     - **1.0**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for FRED 2.42.0
   * -
     -
     - :doc:`AdminManual/Configuration` |BR|
       :doc:`Concepts/LifeCycle/FormalRules`
     - Domain life cycle paramaters moved to a new ``domain_lifecycle_parameters`` table |BR|
       Added new variable and commands
   * -
     -
     - :doc:`Concepts/LifeCycle/Domains`  |BR|
     - Some minor rephrasing and minor links changes
   * - **2.41**
     - **1.6**
     - :doc:`README` |BR|
     - Convert README.md to README.rst
   * -
     -
     - :ref:`Diagram of FRED components <fig-arch-components>` |BR|
       :ref:`Component deployment diagram <fig-deployment>`
     - Update components and deployment schemas
   * -
     - **1.5**
     - :doc:`/Architecture/TopLevelComponents/Clients` |BR|
     - Added description of `fred-registry-services` and `fred-logger-services`
   * -
     -
     - :doc:`/Architecture/TopLevelComponents/Servers` |BR|
     - Added description of Ferda service
   * -
     -
     - :doc:`/Architecture/TopLevelComponents/index` |BR|
       :doc:`/Architecture/Deployment`
     - Updated and extended the deployment schema with `Ferda`, `fred-logger-service` and `fred-registry-services`
   * -
     -
     - :doc:`/Features/General/Notifications` |BR|
     - Added link to poll messages
   * -
     -
     - :doc:`/Features/General/RRRModel` |BR|
     - Added link to Ferda's manual
   * -
     - **1.4**
     - :doc:`/FerdaManual/index` |BR|
       :doc:`/FerdaManual/Intro` |BR|
       :doc:`/FerdaManual/Installation` |BR|
       :doc:`/FerdaManual/Configuration` |BR|
       :doc:`/FerdaManual/Localization` |BR|
       :doc:`/FerdaManual/Two-factor Authentication` |BR|
       :doc:`/Features/AdminIF/Ferda`
     - Extended Ferda Administration Manual and Ferda's features section |BR|
       Added Two-factor Authentication section
   * -
     -
     - :doc:`/Architecture/SourceCode`
     - Updated repository paths and added new repositories
   * -
     -
     - :doc:`/AdminManual/Installation/BinsUbuntu`
     - Added fingerprint check to the installation process
       Change in adding required repositories
   * -
     -
     - :doc:`/AdminManual/Customization/WebwhoisTemplates`
     - `webwhois/public_request_form_menu.html` replaced with `webwhois/include/public_request_form_fields.html`
   * -
     -
     - :doc:`/RDAPReference/HTTPStatuses`
     - Added a new section HTTP Statuses
   * -
     - **1.3**
     - :doc:`/AdminManual/Installation/SystemReqs`,
       :doc:`/AdminManual/Installation/BinsFedora`
     - Added support for RHEL/CentOS 8
   * -
     - **1.2**
     - :doc:`/ReleaseNotes/index`
     - Expanded release notes for FRED 2.41.0
   * -
     -
     - :doc:`/Architecture/SourceCode`,
       :doc:`/AdminManual/Installation/SourceTar`
     - Reorganized source code structure and installation procedure
   * -
     -
     - :doc:`/EPPReference/CommandStructure/Create/CreateKeyset`
     - Fixed minimum occurences of ``<keyset:dnskey>`` element in the EPP command
   * -
     - **1.1**
     - :doc:`/FerdaManual/index`
     - Added Ferda Administration Manual
   * -
     - **1.0**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for FRED 2.41.0
   * -
     -
     - :doc:`/Architecture/TopLevelComponents/index`
     - Replaced the diagram with clickable SVG
   * -
     -
     - :doc:`/AdminManual/Installation/SystemReqs`
     - Added support for Fedora 31
   * -
     -
     - :doc:`/EPPReference/Introduction/index`
     - Added listing of EPP specifications
   * -
     -
     - :doc:`/RDAPReference/index`
     - Added status mapping reference
   * - **2.40**
     - **1.1**
     - :doc:`/AdminManual/Installation/index`
     - Corrected installation procedures
   * -
     - **1.0**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for FRED 2.40.0
   * -
     -
     - :doc:`/LegalNotice`
     - Added legal notice for the FRED and the documentation
   * -
     -
     - :ref:`Object modif. (Communication) <comm-objmodif>`, |br|
       :ref:`Object update (Poll message types) <epp-poll-type-update>`
     - Added a new communication rule (notify a registrar of a domain about
       an update in a linked contact of another registrar)
   * - **2.39**
     - **1.4**
     - :doc:`/EPPReference/CommandStructure/Poll/MessageTypes`
     - Reworked this chapter a little, added more examples
   * -
     - **1.3**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for FRED 2.39.1 and 2.39.2
   * -
     -
     - :doc:`/AdminManual/Installation/SystemReqs`
     - Upgraded supported Fedora versions
   * -
     - **1.2**
     - :doc:`/AdminManual/Installation/BinsUbuntu`,
       :doc:`/AdminManual/Installation/BinsFedora`
     - Updated installation procedures - system registrar required for servers to launch
   * -
     - **1.1**
     - :doc:`/AdminManual/Customization/Web`
     - Added webwhois customization and template reference
   * -
     - **1.0**
     - :doc:`/Architecture/SourceCode`
     - Added libfred component and updated build groups
   * -
     -
     - :doc:`/AdminManual/Installation/SourceTar`
     - Updated installation procedure with new tools
   * -
     -
     - :doc:`/AdminManual/AdministrativeTasks/Objects`
     - Contact unblocking together with domain now possible
   * -
     -
     - :doc:`/AdminManual/Customization/index`
     - Customization reworked for email templates and state-change notifications
   * - **2.38**
     - **1.4**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for FRED 2.38.{2,3,4,5} and FRED 2.39.0
   * -
     - **1.3**
     - :ref:`system-reqs`
     - Updated supported Fedora versions
   * -
     -
     - :doc:`/AdminManual/Installation/BinsUbuntu`,
       :doc:`/AdminManual/Installation/BinsFedora`
     - Updated installation procedures
   * -
     - **1.2**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for FRED 2.38.1
   * -
     - **1.1**
     - :doc:`/ReleaseNotes/index`
     - Corrected the note in 2.38.0 about the ``sendauthinfo`` bugfix
   * -
     - **1.0**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for FRED 2.38.0, 2.37.3 and 2.37.2
   * -
     -
     - :doc:`/ReleaseNotes/Upgrade-2-38`
     - Added considerations before upgrade
   * -
     -
     - :ref:`features-gen-billing`, |br|
       :doc:`/Concepts/Billing`, |br|
       :doc:`/Concepts/PAIN`, |br|
       :doc:`/Architecture/BlackboxModel`, |br|
       :doc:`/Architecture/TopLevelComponents/index`, |br|
       :doc:`/AdminManual/Configuration`, |br|
       :ref:`cron-collect-payments`, |br|
       :ref:`daphne-task-assign-payment`
     - Added or changed according to PAIN Phase 1
       (see :doc:`the release notes </ReleaseNotes/index>`)
   * -
     -
     - :ref:`contact-disclosure`,
       :ref:`config-contact-disclosure`,
       :doc:`/EPPReference/PoliciesRules`
     - Changed disclosure policies to configurable
   * -
     -
     - :ref:`install-dist`
     - Marked more packages as ported to setuptools
   * -
     -
     - :ref:`FRED-Admin-reginit-zone-ns`
     - Changed syntax of the command
   * -
     -
     - :ref:`resolve-public-request`
     - Changed the name of the status of new public requests
   * -
     -
     - :ref:`config-dbparams`
     - Revised configuration of basic :term:`db` parameters
   * -
     -
     - /AdminManual/Extensions,
       :doc:`/Architecture/TopLevelComponents/Clients`
     - Removed :term:`CZ-specific` front-end extensions,
       because they are not released to the public
   * -
     -
     - :doc:`/AdminManual/Installation/BinsUbuntu`
     - Revised the installation process a tiny bit
   * - **2.37**
     - **1.6**
     - :ref:`Audit log feature <features-gen-auditlog>`,
       :doc:`Audit log concept </Concepts/AuditLog>`
     - Added the audit log
   * -
     - **1.5**
     - :doc:`/Concepts/UsersInterfaces`
     - Added an introduction to FRED's users and user interfaces
   * -
     -
     - :doc:`/Concepts/Communication`
     - Added an overview of FRED's communication (notifications, warnings, etc.)
   * -
     -
     - :doc:`/Features/PublicIF/index`
     - Added a list of Public interface features
   * -
     - **1.4**
     - :doc:`/Concepts/EPPClientWorkflow`
     - Added a description of a general EPP client workflow
   * -
     -
     - :doc:`/RDAPReference/index`
     - Added an RDAP reference guide

   * -
     -
     - :doc:`/Architecture/Deployment`
     - Added an example of distributed deployment
   * -
     - **1.3**
     - :ref:`config-contact-reminder`
     - Added a configurable database table
   * -
     -
     - :doc:`/Concepts/Billing`
     - Added a very general description of handling money in the FRED
   * -
     -
     - :doc:`/AdminManual/Appendixes/EmailParameters`
     - Reviewed mail template parameters
   * -
     - **1.2**
     - :doc:`/AdminManual/Installation/SystemReqs`
     - Discontinued support for Ubuntu 14
   * -
     -
     - :doc:`/AdminManual/Installation/BinsUbuntu`
     - Updated the installation script and its description
   * -
     - **1.1**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for the version 2.37.1
   * -
     -
     - :doc:`/ReleaseNotes/Upgrade-2-37`
     - Added considerations before upgrading
   * -
     -
     - :doc:`/Concepts/ContactMerger`
     - Corrected the definition of identical contacts
   * -
     -
     - :ref:`cronjob-contact-merger`
     - Added a cronjob
   * -
     - **1.0**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for FRED 2.37.0
   * -
     -
     - :doc:`/Features/General/index`
     - Added GDPR compliance as a new FRED feature
   * -
     -
     - :doc:`/EPPReference/PoliciesRules`
     - Added a new chapter
   * -
     -
     - :doc:`/EPPReference/CommandStructure/Create/CreateContact`,
       :doc:`/EPPReference/CommandStructure/Update/UpdateContact`,
       :doc:`/EPPReference/CommandStructure/Info/InfoContact`
     - Improved explanations about information disclosure
   * -
     -
     - :ref:`epp-poll-type-update`
     - Added a poll-message type about contact update
   * -
     -
     - :doc:`/AdminManual/AdministrativeTasks/Objects`
     - Added a new public-request type
   * -
     -
     - :ref:`cronjob-public-requests`
     - Added a cronjob to process public requests for personal information
   * -
     -
     - :doc:`/AdminManual/Appendixes/EmailParameters`
     - Added a new email template for sending personal information
   * - **2.36**
     - **1.2**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for the version 2.36.1; upgraded to a newer Sphinx
   * -
     - **1.1**
     - :doc:`/AdminManual/Installation/SourceTar`
     - Upgraded installation procedure to use source from GitHub,
       new signing key for secure apt
   * -
     - **1.0**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for FRED 2.36
   * -
     -
     - :doc:`/Concepts/index`
     - Extracted to a separate publication
   * -
     -
     - :doc:`/Concepts/LifeCycle/index`
     - Added object life cycle
   * -
     -
     - :doc:`/Concepts/Contacts`
     - Added contacts
   * - **2.35**
     - **1.0**
     - :doc:`/ReleaseNotes/index`
     - Added release notes for FRED 2.35
   * -
     -
     - :doc:`/ReleaseNotes/Upgrade-2-35`
     - An ad-hoc guide to database upgrade specifics in this release
   * -
     -
     - :doc:`System requirements </AdminManual/Installation/SystemReqs>`
     - Increased minimum version of PostgreSQL
   * -
     -
     - :doc:`Customization </AdminManual/Customization/Email>`,
       :doc:`Email Params </AdminManual/Appendixes/EmailParameters>`
     - Changed email template database table name
   * -
     -
     - :doc:`Features </Features/General/RecordStatements>`,
       :doc:`Features </Features/AdminIF/WebAdmin>`,
       :doc:`Components </Architecture/TopLevelComponents/index>`,
       :ref:`Components <FRED-Arch-servers-rsif>`,
       :ref:`Task <generate-rs>`
     - Generation of historical record statements in Daphne
   * -
     -
     - :doc:`Features admin </Features/AdminIF/CLIAdmin>`
     - New administration feature to manage objects
   * -
     -
     - :doc:`Source code </Architecture/SourceCode>`
     - Added list of GitHub repositories
   * -
     -
     - :ref:`ORB parameters <config-servers-omni>`
     - Added minimum omniORB settings for FRED servers
   * - **2.34**
     - **1.1**
     - :doc:`/Concepts/ContactMerger` and :ref:`contact-merge`
     - Criteria of destination contact selection in an automatic merger, some minor rephrasing
   * -
     -
     - :doc:`/EPPReference/CommandStructure/Update/UpdateDomain`
     - Mention of nsset and keyset unlinking with empty elements
   * -
     - **1.0**
     - :doc:`/ReleaseNotes/index`
     - Added release notes
   * -
     -
     - :doc:`Diagram of FRED components </Architecture/TopLevelComponents/index>`
     - Removed dependency on ``fred-logd`` from ``fred-pifd``
   * -
     -
     - :ref:`cronjob-regular` and :ref:`cronjob-object-deletion`
     - Procedures accept object types by name, new argument, removed dependency on ``fred-rifd``
   * - **2.33**
     - **1.2**
     - :doc:`/EPPReference/ManagedObjects/index`
     - Added divergence from the standards of object mapping in FRED EPP
   * -
     -
     - :doc:`/Concepts/Teccheck`
     - Expanded on the concept of technical checks
   * -
     - **1.1**
     - :doc:`/Features/General/index`
     - Added record statements feature, component and email template
   * -
     - **1.0**
     - :doc:`/EPPReference/index`
     - Added mailing address extension of contacts
   * -
     -
     - :doc:`/EPPReference/Appendixes/ErrorReasons`
     - New texts of EPP error reasons
   * - **2.32**
     -
     - :ref:`config-handles`
     - Added configurable handle format validation
   * -
     -
     - :ref:`config-dn`
     - Added configurable domain name format validation
   * - **2.31**
     -
     - :doc:`/Concepts/AKM`
     - Added :term:`AKM` concept, components, task and email templates
   * - **2.30**
     -
     - :doc:`/Concepts/ContactMerger`
     - Added contact merger concept, tasks and email template
   * -
     -
     - :doc:`/AdminManual/Appendixes/EmailParameters`
     - Added a new CS parameter
   * - older
     -
     - :doc:`/AdminManual/Appendixes/EmailParameters`
     - Added more email templates
