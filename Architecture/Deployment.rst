.. _FRED-Arch-Deployment:

Distributed deployment example
-------------------------------

Deploying FRED on multiple servers brings at least two advantages:

* increased performance,
* access control on the network level.

Deploying on multiple physical servers is not the only distributed solution,
deploying on virtual servers or separating tasks on the process level is also
possible.

.. rubric:: Nodes overview

Nodes in this document represent execution environments.

We work with the following nodes:

* `EPP node`_ -- EPP service
* `ADMIN node`_ -- web admin service
* `WEB node`_ -- public web services: Unix WHOIS, Web WHOIS, RDAP
* `HM node`_ -- zone management
* `APP node`_ -- application servers, CLI admin tools, pgbouncer, CORBA naming
  service
* :ref:`DB node <deploy-db>` -- the main FRED database
* :ref:`LOGDB node <deploy-db>` -- the logger database
* `Secretary node`_
* `Messenger node`_

.. Tip::

   .. rubric:: Redundancy

   This text does not describe redundancy options in detail, but here is a quick tip:

   * database replication is a standard technique to protect data,
   * the whole system can be replicated in several instances on different
     localities, which can substitute one another when one instance fails or
     during a system upgrade.

.. contents::
   :local:
   :backlinks: none

.. _deploy-network:

Network
^^^^^^^^

Network rules are described per node in the following sections, but here is an
overview of logical connections in the network (a single instance of the system).

.. _deploy-ports:

.. _fig-deployment-network:

.. figure:: /Architecture/_graphics/schema-network.png
   :alt:
   :align: center
   :figwidth: 100%

   Network -- Logical topology

*Restricted network access* means that servers should be accessed only from IP
addresses and ports allowed on a firewall.

*Unrestricted network access* means that servers can be accessed from any IP
address, but only necessary ports should be open for access as illustrated
in the network rules for each node.

.. _deploy-epp:

EPP node
^^^^^^^^^^^

Provides: EPP service

Packages:

* libapache2-mod-corba
* libapache2-mod-eppd;

Network:

* access to EPP (tcp, port 700) permitted only from particular IP addresses
  (or ranges) declared by registrars

.. list-table:: EPP node packages
   :widths: 10 20 20 20
   :header-rows: 1

   * - Package
     - Provided services
     - Description
     - Default config location
   * - ``libapache2-mod-corba``
     - None (is only an Apache module)
     - Apache module that provides common functionality of CORBA communication for EPP and WHOIS Apache modules
     - ``/etc/apache2/sites-enabled`` (generated after module activation)
   * - ``libapache2-mod-eppd``
     - None (is only an Apache module)
     - Apache module for parsing EPP commands and transforming them into CORBA calls to server (and vice versa)
     - ``/etc/apache2/sites-available/02-fred-mod-eppd-apache.conf`` (generated after module activation)

.. _deploy-admin:

ADMIN node
^^^^^^^^^^^

 Provides: Web administration interface

Network:

* access to HTTPS (tcp, port 443) permitted only from the private network of
  the Registry

.. list-table:: ADMIN node docker images
   :widths: 10 20 20
   :header-rows: 1

   * - Docker image
     - Description
     - Default config location
   * - ``ferda-nginx``
     - http webserver
     - none, configuration example in :repo:`fred/ferda/-/tree/master/docs/demo-deploy`
   * - ``ferda-uwsgi``
     - webserver gateway interface
     - none, configuration example in :repo:`fred/ferda/-/tree/master/docs/demo-deploy`

.. _deploy-web:

WEB node
^^^^^^^^^^^

Provides: Unix WHOIS, Web WHOIS, RDAP

Network:

* access to HTTPS (tcp, port 443) permitted from anyone
* access to WHOIS (tcp, port 43) permitted from anyone


.. list-table:: WEB node packages
   :widths: 10 20 20 20
   :header-rows: 1

   * - Package
     - Provided services
     - Description
     - Default config location
   * - ``libapache2-mod-corba``
     - No provided services (is only an Apache module)
     - Apache module that provides common functionality of CORBA communication for EPP and WHOIS Apache modules
     - ``/etc/apache2/sites-enabled`` (generated after module activation)
   * - ``libapache2-mod-whoisd``
     - No provided services (is only an Apache module)
     - Apache module for parsing EPP commands and transforming them into CORBA calls to server (and vice versa)
     - ``/etc/apache2/sites-available/02-fred-mod-whoisd-apache.conf`` (generated after module activation)

.. list-table:: WEB node docker images
   :widths: 10 20 20
   :header-rows: 1

   * - Docker image
     - Description
     - Default config location
   * - ``rdap-nginx``
     - http webserver
     - none, configuration example in :repo:`fred/ferda/-/tree/master/docs/demo-deploy`
   * - ``rdap-uwsgi``
     - webserver gateway interface
     - none, configuration example in :repo:`fred/ferda/-/tree/master/docs/demo-deploy`
   * - ``webwhois-nginx``
     - http webserver
     - none, configuration example in :repo:`fred/ferda/-/tree/master/docs/demo-deploy`
   * - ``webwhois-uwsgi``
     - webserver gateway interface
     - none, configuration example in :repo:`fred/ferda/-/tree/master/docs/demo-deploy`

.. _deploy-hm:

HM node
^^^^^^^^^^

Hidden master for the DNS infrastructure.

Provides: zone file generation, zone signing, DNS servers notification

Network:

* access to IXFR (tcp, port 53) permitted only from DNS servers

.. list-table:: HM node packages 
   :widths: 10 20 20 20
   :header-rows: 1

   * - Package
     - Provided services
     - Description
     - Default config location
   * - ``fred-zone-generator``
     - None
     - System binary for zonefile generation
     - ``/etc//fred/fred-zone-generator.conf``

.. _deploy-app:

APP node
^^^^^^^^^^^

Provides:

* CORBA naming service (omninames) as a virtual server "corba",
* backend application servers,
* CLI administration tools,

.. Tip::

    In addition to FRED components we recommend adding :program:`pgbouncer` for database connection distribution.

Network:

* only internal access from the private network of the Registry

.. list-table:: APP node packages 
   :widths: 10 20 20 20
   :header-rows: 1

   * - Package
     - Provided services
     - Description
     - Default config location
   * - ``fred-accifd``
     - ``/lib/systemd/system/fred-accifd.service``
     - FRED backend for accounting
     - ``/etc/init/fred-accifd.conf``
   * - ``fred-adifd``
     - ``/lib/systemd/system/fred-adifd.service``
     - Administration interface daemon
     - ``/etc/init/fred-adifd.conf``
   * - ``fred-akm``
     - None
     - FRED automatic keyset management client
     - ``etc/fred/fred-akm.conf``  
   * - ``fred-akmd``
     - ``/lib/systemd/system/fred-akmd.service``
     - FRED backend for automatic keyset management
     - ``/etc/init/fred-akmd.conf``
   * - ``fred-api-fileman``
     - None
     - FRED fileman services interface definition files
     - None
   * - ``fred-api-logger``
     - None
     - FRED logger services interface definition files
     - None
   * - ``fred-backend-dbreport``
     - ``/lib/systemd/system/fred-dbreport-services.service`` |BR|
       ``/lib/systemd/system/fred-dbreport-services@.service``
     - FRED server for database reports (gRPC)
     - None
   * - ``fred-backend-fileman``
     - ``/lib/systemd/system/fred-fileman-server.service``
     - FRED service for file management
     - ``/etc/fred/fileman.conf``
   * - ``fred-backend-logger``
     - ``/lib/systemd/system/fred-backend-logger.service``
     - FRED logger services (gRPC)
     - None
   * - ``fred-backend-logger-corba``
     - ``/lib/systemd/system/fred-backend-logger-corba.service``
     - FRED logger services (CORBA)
     - None
   * - ``fred-backend-notify``
     - None
     - FRED notify implementation
     - ``/etc/fred/fred-notify-contact-data-reminder-example.conf`` |BR|
       ``/etc/fred/fred-notify-object-events-example.conf`` |BR|
       ``/etc/fred/fred-notify-object-state-changes-example.conf``
   * - ``fred-backend-public-request``
     - ``/lib/systemd/system/fred-backend-public-request.service``
     - FRED backend for public requests management
     - None
   * - ``fred-backend-registry``
     - ``/lib/systemd/system/fred-backend-registry.service``
     - FRED registry core services (gRPC)
     - None
   * - ``fred-backend-zone``
     - ``/lib/systemd/system/fred-zone-services.service``
     - FRED backend service for DNS zone generator
     - ``etc/fred/fred-zone-services.conf``
   * - ``fred-idl``
     - None
     - FRED server interface definition files
     - None
   * - ``fred-pifd``
     - ``/lib/systemd/system/fred-pifd.service``
     - FRED public interface daemon
     - ``/etc/init/fred-pifd.conf``    
   * - ``fred-rifd``
     - ``/lib/systemd/system/fred-rifd-services.service``
     - FRED registrar interface daemon
     - ``/etc/init/fred-rifd.conf``    
   * - ``cdnskey-scanner``
     - None
     - CDNSKEY records scanner
     - None
   * - ``python3-pydantic``
     - None
     - Data validation and settings management using python type hinting
     - None   

.. _deploy-db:

Database nodes
^^^^^^^^^^^^^^^^^

Database is separated into several nodes:

* DB -- the main database ``freddb`` -- data of all domains, contacts, registrars, history etc.
* LOGDB -- the :doc:`audit log (logger) </Concepts/AuditLog>` database ``logdb`` -- logging of all
  user transactions
* messenger
* secretary
* FERDA

We have the logger database separately due to high workload.

Network:

* accessed only by the backend server(s) from the :ref:`APP node <deploy-app>`

.. list-table:: DB node packages 
   :widths: 10 20 20 20
   :header-rows: 1

   * - Package
     - Provided services
     - Description
     - Default config location
   * - ``fred-db``
     - None
     - Database schema and example data for FRED
     - None
   * - ``postgresql-13``
     - None
     - PostgreSQL database server
     - None

.. _deploy-secretary:

Secretary node
^^^^^^^^^^^^^^^^^^

.. list-table:: Secretary node packages 
   :widths: 10 20 20 20
   :header-rows: 1

   * - Package
     - Provided services
     - Description
     - Default config location
   * - ``python3-django-secretary``
     - ``/usr/share/doc/python3-django-secretary/examples/fred-secretary.service``
     - Django app for rendering e-mails and PDFs
     - ``/usr/share/doc/python3-django-secretary/examples/settings.py``
   * - ``nginx-full``
     - None
     - http web server
     - None
   * - ``uwsgi``
     - None
     - webserver gateway interface
     - None


.. _deploy-messenger:

Messenger node
^^^^^^^^^^^^^^^^^^

.. list-table:: Messenger node packages 
   :widths: 10 20 20 20
   :header-rows: 1

   * - Package
     - Provided services
     - Description
     - Default config location
   * - ``fred-api-fileman``
     - None
     - FRED fileman services interface definition files
     - None
   * - ``fred-api-messenger``
     - None
     - FRED messenger services interface definition files
     - None
   * - ``fred-backend-messenger``
     - ``/lib/systemd/system/fred-messenger-server.service`` |BR|
       ``/lib/systemd/system/fred-messenger-sender-email.service`` |BR|
       ``/lib/systemd/system/fred-messenger-sender-sms.service`` 
     - FRED service for sending and archiving messages
     - None
