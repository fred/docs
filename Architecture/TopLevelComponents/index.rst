
.. _FRED-Arch-TopComponents:

Top-level components
===============================

.. only:: mode_structure

   .. struct-start

   **Sources:** OLDREPO + NOTES
   | **AoW:** 3 days = *2 days (base on old picture)*

   **Chapter outline:**

   * Servers, Clients, user agents
   * internal interfaces (CORBA IDL)

   .. struct-end

This chapter contains diagrams and brief descriptions of FRED top-level
components and their relationships.

.. _fig-arch-components:

.. rubric:: Diagram of FRED components (For full resolution `Click here <https://fred.nic.cz/documentation/html/_images/schema-components.png>`_)

.. figure:: ../_graphics/schema-components.png
   :alt:
   :align: center
   :figwidth: 100%

.. rubric:: Legend

* *Arrows* signify direct cooperation of components (the arrow points
  at the component which serves the other component); neither the colour
  nor the style of arrows carry any meaning.

For pin list of all necessary packages see the latest release notes. 

.. toctree::
   :maxdepth: 2
   :caption: Chapter TOC

   UserAgents
   Clients
   Servers
   Databases
